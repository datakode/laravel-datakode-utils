<?php

return [
    'process_end_email_title' => 'Téléchargez le résultat du traitement',

    'process_end_email_subject.process_export' => 'Votre export est terminé',
    'process_end_email_subject.process_pdf_export' => 'Votre export PDF est terminé',

    'unknown-fields-error' => "Le champs :field n'est pas importable",
    'not-writable-fields-error' => "Vous n'avez pas les droits d'écriture concernant le champs :field",
];
