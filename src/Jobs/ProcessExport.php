<?php

namespace Datakode\LaravelDatakodeUtils\Jobs;

use Datakode\LaravelDatakodeUtils\Services\ExportService;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProcessExport extends AbstractProcess implements ShouldBeUnique, ShouldQueue
{
    /**
     * Execute the job.
     */
    public function handle(ExportService $export): void
    {
        $this->setCurrentUser();
        $export->runExportJob($this);
    }

    /**
     * Get the unique ID for the job.
     */
    public function uniqueId(): string
    {
        return (string) $this->processing->id;
    }
}
