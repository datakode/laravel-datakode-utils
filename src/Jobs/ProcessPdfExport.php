<?php

namespace Datakode\LaravelDatakodeUtils\Jobs;

use Datakode\LaravelDatakodeUtils\DatakodeQueryBuilder;
use Datakode\LaravelDatakodeUtils\LaravelData\DatakodeDataBag;
use Datakode\LaravelDatakodeUtils\Model\Processing;
use Datakode\LaravelDatakodeUtils\Services\ExportPdfService;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\App;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

abstract class ProcessPdfExport extends AbstractProcess implements ShouldBeUnique, ShouldQueue
{
    public function __construct(Processing $processing, DatakodeQueryBuilder $queryBuilder, Authenticatable $authenticatable, array $parameters)
    {
        parent::__construct($processing, $queryBuilder, $authenticatable, $parameters);
        $this->dataClass = new DatakodeDataBag(
            $parameters['dataClass']
        ); // set dataclass from params to get custom export dataclass
    }

    /**
     * method to be called on handle job
     *
     *  |  you need to extend this class and use you service in handle like this:
     *  |
     *  |  public function handle(MyExportPdfService $export): void
     *  |  {
     *  |     $this->run($export);
     *  |  }
     *  |
     *  |  where MyExportPdfService extends Datakode\LaravelDatakodeUtils\Services\ExportPdfService
     */
    public function run(ExportPdfService $export): void
    {
        $this->setCurrentUser();
        $this->checkMaxElements();
        $export->runExportPdfJob($this);
    }

    /**
     * Get the unique ID for the job.
     */
    public function uniqueId(): string
    {
        return (string) $this->processing->id;
    }

    protected function checkMaxElements(): void
    {
        $maxExport = config('laravel-datakode-utils.exports.pdf.max');
        if ($maxExport) {
            $count = $this->getEloquentBuilder()
                ->toBase()
                ->getCountForPagination();
            if ($count > $maxExport) {
                App::abort(
                    HttpResponse::HTTP_BAD_REQUEST,
                    "L'export PDF est limité à {$maxExport} éléments. " . $count . ' éléments demandés.'
                );
            }
        }
    }
}
