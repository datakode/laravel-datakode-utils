<?php

namespace Datakode\LaravelDatakodeUtils\Jobs;

use AnourValar\EloquentSerialize\Facades\EloquentSerializeFacade;
use Datakode\LaravelDatakodeUtils\DatakodeQueryBuilder;
use Datakode\LaravelDatakodeUtils\LaravelData\DatakodeDataBag;
use Datakode\LaravelDatakodeUtils\Model\Processing;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Throwable;

abstract class AbstractProcess
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public string $format;

    public DatakodeDataBag $dataClass;

    public string $filename;

    public string $model;

    protected string $package;

    public string|null $translateKey;

    public bool $flatten;

    /**
     * Create a new job instance.
     */
    public function __construct(
        public Processing $processing,
        DatakodeQueryBuilder $queryBuilder,
        public Authenticatable $authenticatable,
        public array $parameters
    ) {
        $eloquentBuilder = $queryBuilder->getEloquentBuilder();
        $this->package = EloquentSerializeFacade::serialize($eloquentBuilder);
        $this->filename = $queryBuilder->getExportFilename();
        $this->dataClass = $queryBuilder->getExportDataClass();
        $this->format = Arr::get($this->parameters, 'format');
        $this->translateKey = Arr::get($this->parameters, 'translateKey');
        $this->flatten = Arr::get($this->parameters, 'flatten', false);
        $this->model = $queryBuilder::$model;
    }

    public function getEloquentBuilder(): Builder
    {
        return EloquentSerializeFacade::unserialize($this->package);
    }

    /**
     * Handle a job failure.
     */
    public function failed(Throwable $exception): void
    {
        $this->processing->status = 'failed';
        $this->processing->info = $exception->getMessage();
        $this->processing->save();
    }

    /**
     * set current user for process
     */
    protected function setCurrentUser(): void
    {
        Auth::setUser($this->authenticatable);
    }
}
