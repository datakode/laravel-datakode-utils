<?php

namespace Datakode\LaravelDatakodeUtils;

use Datakode\LaravelDatakodeUtils\Model\DatakodeUser;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use MatanYadaev\EloquentSpatial\Enums\Srid;
use MatanYadaev\EloquentSpatial\Objects\Geometry;
use Override;
use RuntimeException;
use Stringable;

class DatakodeFormRequest extends FormRequest
{
    public ?string $model = null;

    protected array $geometricFields = ['geometry', 'geom'];

    private array $noPostValidationFor = [];

    public function rules(): array
    {
        return [];
    }

    /**
     * return validation rules filtered on user field write permissions
     */
    public function getFilteredRules(): Collection
    {
        $user = DatakodeAuth::currentUser();
        if (! $user) {
            return collect($this->rules());
        }

        return collect($this->rules())
            ->filter(function ($value, $key) use ($user): bool {
                return $user->hasWriteAccessToField($this->_getModel(), $key);
            });
    }

    /**
     * return validation rules as array of string, filtered on user field write permissions
     */
    public function getRulesArray(): array
    {
        $rules = $this->getFilteredRules()
            ->map(function ($value, $key) {
                if (is_array($value)) {
                    return collect($value)->map(function ($item): string {
                        return (string) $item;
                    })->join('|');
                }

                return $value;
            });

        return $rules->toArray();
    }

    public function getRulesArrayMeta(bool $asObject = false): array
    {
        $rules = $this->getFilteredRules()
            ->map(function ($value, $key) {
                if (is_array($value)) {
                    return collect($value)->map(function ($item) {
                        if (method_exists($item, 'toMetaString')) {
                            return $item->toMetaString();
                        }

                        if ($item instanceof Stringable) {
                            return (string) $item;
                        }

                        return is_string($item) ? $item : class_basename($item::class);
                    })->join('|');
                }

                return $value;
            });

        $res = $rules->toArray();
        if (! $asObject) {
            return $res;
        }

        return collect($rules)
            ->map(fn ($value): array => DatakodeFormRequest::ruleStringToObject($value))
            ->toArray();
    }

    public function getRoute($name): Model|string|null
    {
        /** @var Request $request */
        $request = request();

        return $request->route($name);
    }

    public static function ruleStringToObject($value): array
    {
        $value = collect(explode('|', (string) $value))
            ->mapWithKeys(
                function ($item): array {
                    $split = explode(':', $item);
                    if (count($split) === 1) {
                        return [
                            $item => true,
                        ];
                    }

                    $k = $split[0];
                    $v = Str::replaceFirst($k . ':', '', $item);
                    if (in_array($k, ['in', 'required_with'])) {
                        $v = collect(explode(',', $v))
                            ->map(function ($value) {
                                if (Str::startsWith($value, '"') && Str::endsWith($value, '"')) {
                                    return Str::of($value)->substr(1, -1)->value();
                                }

                                return $value;
                            })
                            ->toArray();
                    }

                    return [
                        $k => $v,
                    ];
                }
            );
        return $value->toArray();
    }

    protected function getCurrentUser(): DatakodeUser
    {
        $user = DatakodeAuth::currentUser();
        if (! $user) {
            throw new RuntimeException('User not found.');
        }

        return $user;
    }

    /**
     * Prepare the data for validation.
     * transform array of geom into MultiPolygon, MultiPoint or MultiLineString
     * @todo handle GeometryCollection object
     */
    #[Override]
    protected function prepareForValidation(): void
    {
        foreach ($this->geometricFields as $geometricField) {
            $geometry = $this->get($geometricField);
            if (! $geometry) {
                continue;
            }

            if (! is_array($geometry)) {
                continue;
            }

            if (! isset($geometry[0])) {
                continue;
            }

            $this->prepareGeomFieldForValidation($geometry, $geometricField);
        }
    }

    #[Override]
    protected function passedValidation(): void
    {
        foreach ($this->geometricFields as $geometricField) {
            $geometry = $this->get($geometricField);
            if (! $geometry) {
                continue;
            }

            if (in_array($geometricField, $this->noPostValidationFor)) {
                continue;
            }

            try {
                $this->merge([
                    $geometricField => Geometry::fromJson(json_encode($geometry), Srid::WGS84->value),
                ]);
            } catch (Exception) {
                $this->merge([
                    $geometricField => 0,
                ]);
            }
        }
    }

    private function prepareGeomFieldForValidation(array $geometry, mixed $geometricField): void
    {
        $geometries = [];
        /** @var string|null $type */
        $type = null;

        foreach ($geometry as $feature) {
            $geometries[] = Arr::get($feature, 'geometry.coordinates');
            switch (Arr::get($feature, 'geometry.type')) {
                case 'Polygon':
                    $type = 'MultiPolygon';
                    break;
                case 'Point':
                    $type = 'MultiPoint';
                    break;
                case 'LineString':
                    $type = 'MultiLineString';
                    break;
            }
        }

        $geometryCollection = [
            'type' => $type,
            'coordinates' => $geometries,
        ];
        try {
            $data = Geometry::fromJson(json_encode($geometryCollection), Srid::WGS84->value);
        } catch (Exception) {
            $data = 1;
        }

        $this->merge([
            $geometricField => $data,
        ]);
        $this->noPostValidationFor[] = $geometricField;
    }

    private function _getModel(): string
    {
        if (! $this->model) {
            throw new RuntimeException('FormRequest ' . static::class . ' $model must be set.');
        }

        return $this->model;
    }
}
