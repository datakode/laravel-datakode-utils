<?php

namespace Datakode\LaravelDatakodeUtils\Requests;

use Datakode\LaravelDatakodeUtils\DatakodeFormRequest;
use Datakode\LaravelDatakodeUtils\Model\Processing;
use Override;

class ProcessingRequest extends DatakodeFormRequest
{
    public ?string $model = Processing::class;

    #[Override]
    public function rules(): array
    {
        return [
            'mail_me' => 'boolean|required',
        ];
    }
}
