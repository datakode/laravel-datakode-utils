<?php

namespace Datakode\LaravelDatakodeUtils;

use Datakode\LaravelDatakodeUtils\Console\Commands\ClearTmpFolder;
use Datakode\LaravelDatakodeUtils\Console\Commands\DbDump;
use Datakode\LaravelDatakodeUtils\Events\ProcessingEndEvent;
use Datakode\LaravelDatakodeUtils\Import\BaseImport;
use Datakode\LaravelDatakodeUtils\Listeners\DiagnosingHealthListener;
use Datakode\LaravelDatakodeUtils\Listeners\SendProcessEndNotificationListener;
use Illuminate\Foundation\Console\AboutCommand;
use Illuminate\Foundation\Events\DiagnosingHealth;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Schedule;
use Illuminate\Support\ServiceProvider;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;
use Override;

class LaravelDatakodeUtilsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    #[Override]
    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/laravel-datakode-utils.php', 'laravel-datakode-utils');
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        // publish config
        $this->publishes([
            __DIR__ . '/../config/laravel-datakode-utils.php' => config_path('laravel-datakode-utils.php'),
        ]);

        // load lang
        $this->loadTranslationsFrom(__DIR__ . '/../lang', 'laravel-datakode-utils');
        $this->publishes([
            __DIR__ . '/../lang' => $this->app->langPath('vendor/laravel-datakode-utils'),
        ]);

        // load migrations
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');

        // add commands

        if ($this->app->runningInConsole()) {
            $this->commands([ClearTmpFolder::class, DbDump::class]);
        }

        // bind events to listeners
        Event::listen(ProcessingEndEvent::class, SendProcessEndNotificationListener::class);
        Event::listen(DiagnosingHealth::class, DiagnosingHealthListener::class);

        HeadingRowFormatter::extend('custom', function ($value) {
            // Excel import: replace . into _DOT_ in header key
            return BaseImport::escapeDot((string) $value);
        });

        // **********
        //  COMMANDS
        // **********

        $dbDumpCron = config('laravel-datakode-utils.db_dump_cron');
        if ($dbDumpCron) {
            Schedule::command('db-dump')->cron($dbDumpCron);
        }

        // *******
        //  ABOUT
        // *******

        $composerData = json_decode(file_get_contents(__DIR__ . '/../composer.json'), true);
        AboutCommand::add('LaravelDatakodeUtils', fn (): array => [
            'version' => Arr::get($composerData, 'version'),
            ...collect(Arr::dot(config('laravel-datakode-utils')))
                ->map(fn ($item) => json_encode($item))
                ->toArray(),
        ]);
    }
}
