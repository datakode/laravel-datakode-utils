<?php

namespace Datakode\LaravelDatakodeUtils\ModelQueryBuilders;

use Datakode\LaravelDatakodeUtils\DatakodeModelQueryBuilder;
use Datakode\LaravelDatakodeUtils\Model\Processing;
use Illuminate\Database\Eloquent\Builder;
use Override;

/**
 * This file is only a sample of use of DatakodeModelQueryBuilder
 * needed to avoid false error reporting "Cannot call abstract method"
 */

class ExampleQueryBuilder extends DatakodeModelQueryBuilder
{
    #[Override]
    public static function getModelClass(): string
    {
        return Processing::class;
    }

    #[Override]
    protected static function getBuilderQuery(): Builder
    {
        return Processing::query();
    }

    #[Override]
    protected function getAllowedSorts(): array
    {
        return ['id', 'name', 'created_at'];
    }

    #[Override]
    protected function getAllowedFilters(): array
    {
        return ['name'];
    }

    #[Override]
    protected function getDefaultSort(): ?string
    {
        return 'name';
    }
}
