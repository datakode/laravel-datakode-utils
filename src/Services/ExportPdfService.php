<?php

namespace Datakode\LaravelDatakodeUtils\Services;

use Barryvdh\DomPDF\Facade\Pdf;
use Datakode\LaravelDatakodeUtils\Jobs\ProcessPdfExport;
use Datakode\LaravelDatakodeUtils\Model\DatakodeApiModel;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

abstract class ExportPdfService extends ExportService
{
    abstract public function getTemplate(ProcessPdfExport $job): string;

    public function runExportPdfJob(ProcessPdfExport $job): void
    {
        $queryBuilder = $job->getEloquentBuilder();

        $this->setJob($job);
        $this->progress(0, 'Initialisation');

        $processing = $job->processing;
        $processing->filename = $job->filename . '_' . Carbon::now()->format('Y-m-d_His')
            . ($job->format === 'pei_sheet_pdf' ? '_fiche_terrain' : '')
            . '.pdf';
        $processing->mime_type = 'application/pdf';

        $processing->save();

        $collection = $queryBuilder->get();

        $template = $this->getTemplate($job);

        $res = $this->_buildPdf($collection, $template);

        $this->jobInfo('Enregistrement du fichier');
        $storage = $processing->store($res, $processing->mime_type, $processing->filename);

        $this->jobInfo('Terminé');
        $processing->end($storage);
    }

    private function _buildPdf(Collection|array $collection, string $template): Response
    {
        $this->progressMax($collection->count() * (1 + 30 / 10000));
        $html = '';
        $nb = 0;
        /** @var DatakodeApiModel $model */
        foreach ($collection->all() as $model) {
            $dataClass = $this->job->dataClass;
            try {
                $data = $dataClass->getData($model)
                    ->toArray();
                $html .= view($template, compact('data'));
            } catch (Exception $e) {
                Log::error(self::class . ' ' . $e->getMessage(), $e->getTrace());
            }

            $nb++;
            $this->progress($nb, 'Construction pages (' . $nb . '/' . $collection->count() . ')');
        }

        return Pdf::loadHTML($html)->download($this->job->filename);
    }
}
