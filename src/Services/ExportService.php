<?php

namespace Datakode\LaravelDatakodeUtils\Services;

use Datakode\LaravelDatakodeUtils\DatakodeApiResponse;
use Datakode\LaravelDatakodeUtils\DatakodeExport;
use Datakode\LaravelDatakodeUtils\DatakodeQueryBuilder;
use Datakode\LaravelDatakodeUtils\Jobs\AbstractProcess;
use Datakode\LaravelDatakodeUtils\Jobs\ProcessExport;
use Datakode\LaravelDatakodeUtils\LaravelData\DatakodeDataBag;
use Datakode\LaravelDatakodeUtils\Model\DatakodeApiModel;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use RuntimeException;
use Shapefile\Geometry\Geometry;
use Shapefile\ShapefileWriter;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use ZipArchive;

class ExportService
{
    protected ?AbstractProcess $job = null;

    private bool $_translateHeading = true;

    private string|null $_translateKey = null;

    public function runExportJob(ProcessExport $job): void
    {
        $queryBuilder = $job->getEloquentBuilder();

        $this->setJob($job);
        $this->progress(0, 'Initialisation');

        $processing = $job->processing;
        $extension = match ($job->format) {
            'shape' => 'zip',
            default => $job->format,
        };
        $processing->filename = $job->filename . '_' . Carbon::now()->format('Y-m-d_His') . '.' . $extension;
        $processing->mime_type = match ($extension) {
            'json' => 'application/json',
            'csv' => 'text/csv',
            'geojson' => 'application/geo+json',
            'zip' => 'application/zip',
            'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
            'xls' => 'application/vnd.ms-excel',
            'html' => 'text/html',
            'pdf' => 'application/pdf',
            default => 'text/plain',
        };

        $processing->save();

        $collection = $queryBuilder->get();
        $res = $this->exportData(
            format: $job->format,
            filename: $job->filename,
            model: $job->model,
            collection: $collection,
            exportDataClass: $job->dataClass,
            translate_key: $job->translateKey,
            flatten: $job->flatten,
        );

        $this->jobInfo('Enregistrement du fichier');
        $storage = $processing->store($res, $processing->mime_type, $processing->filename);

        $this->jobInfo('Terminé');
        $processing->end($storage);
    }

    public function exportData(
        string $format,
        string $filename,
        string $model,
        Collection $collection,
        DatakodeDataBag $exportDataClass,
        string|null $translate_key = 'exports',
        bool $flatten = false,
    ): Response|BinaryFileResponse {
        $this->_translateKey = $translate_key;
        if ($format === 'geojson' || $format === 'shape') {
            if (! $this->_canExportGeom($model)) {
                throw new RuntimeException('Resource can not been export as Geojson.');
            }

            if ($format === 'shape') {
                return $this->_zippedShapefile($collection, $filename, $exportDataClass, $flatten);
            }

            return $this->_geojson($collection, $filename, $exportDataClass, $flatten);
        }

        $dataClass = $exportDataClass;
        $this->progressMax($collection->count() * (1 + 30 / 10000));
        $data = [];
        $chunkSize = 100;
        $nb = 0;
        $chunks = $collection->chunk($chunkSize);
        foreach ($chunks->all() as $chunk) {
            $data = [...$data, ...$dataClass->getData($chunk)->toArray()];
            $nb += $chunkSize;
            $this->progress($nb, "Lecture des données ({$nb}/{$collection->count()})");
        }

        if ($format === 'json') {
            $this->progress($nb, 'Construction du json');

            return (new DatakodeApiResponse($data))
                ->download('application/json', $filename);
        }

        return $this->_excel($data, "{$filename}.{$format}");
    }

    public function setTranslateHeading(bool $translateHeading): static
    {
        $this->_translateHeading = $translateHeading;
        return $this;
    }

    protected function setJob(AbstractProcess $job): void
    {
        $this->job = $job;
    }

    protected function progressMax(float $value): void
    {
        $this->job?->processing->setMaxCpt((int) round($value));
    }

    protected function progress(int $value, string $info): void
    {
        $this->job?->processing->setCpt($value, $info);
    }

    protected function jobInfo(string $info): void
    {
        $this->job?->processing->setInfo($info);
    }

    private function _excel(array $formattedData, string $filename): BinaryFileResponse
    {
        $datakodeExport = new DatakodeExport(
            data: $formattedData,
            translateHeading: $this->_translateHeading,
            translate_key: $this->_translateKey,
        );

        return Excel::download($datakodeExport, $filename);
    }

    private function _canExportGeom(string $modelClass): bool
    {
        return method_exists($modelClass, 'getGeometryArray');
    }

    private function _geojson(
        Collection $collection,
        string $filename,
        DatakodeDataBag $dataClass,
        bool $flatten = false
    ): Response {

        return DatakodeQueryBuilder::geojsonResponse(
            $collection,
            $filename,
            $dataClass,
            $flatten,
            $this->_translateKey
        );

    }

    private function _zippedShapefile(
        Collection $collection,
        string $filename,
        DatakodeDataBag $dataClass,
        bool $flatten = true
    ): BinaryFileResponse {
        // prepare tmp storage
        Storage::makeDirectory('tmp');
        $tmpDirectory = md5('shape_directory' . time());
        Storage::makeDirectory("tmp/{$tmpDirectory}");

        // prepare shapefile
        $shapefileWriter = new ShapefileWriter(storage_path("app/tmp/{$tmpDirectory}/{$filename}.shp"));
        /** @var DatakodeApiModel $firstEntity */
        $firstEntity = $collection->first();

        // TODO add hasGeometry interface to declare methods ...
        /** @noinspection PhpUndefinedMethodInspection */
        $shapeType = $firstEntity->getShapefileType(); // @phpstan-ignore-line
        $shapefileWriter->setShapeType($shapeType);

        // TODO add hasGeometry interface to declare methods ...
        /** @noinspection PhpUndefinedMethodInspection */
        $prj = $firstEntity->getWktProj();  // @phpstan-ignore-line
        $shapefileWriter->setPRJ($prj);

        $keyMapping = $this->_getShapefileKeyMapping($collection, $dataClass);

        collect($keyMapping)
            ->each(function (array $value) use ($shapefileWriter): void {
                $shapefileWriter->addCharField($value['shapefileKey']);
            });
        $this->buildShapefile($collection, $flatten, $keyMapping, $shapefileWriter, $dataClass);

        // KEEP NEXT LINE TO CLOSE FILE WRITER $shapefileWriter=null;
        /** @noRector \Rector\DeadCode\Rector\Assign\RemoveUnusedVariableAssignRector */
        $shapefileWriter = null;

        // build zipfile
        $zipPath = storage_path("app/tmp/{$tmpDirectory}.zip");
        $zipArchive = new ZipArchive();
        $zipArchive->open($zipPath, ZipArchive::CREATE);

        collect(Storage::files("tmp/{$tmpDirectory}"))->each(function (string $item) use ($zipArchive): void {
            $zipArchive->addFile(storage_path("app/{$item}"), basename($item));
        });
        $zipArchive->close();

        Storage::deleteDirectory("tmp/{$tmpDirectory}");

        return response()->download($zipPath)
            ->deleteFileAfterSend(true);
        // TODO delete after send dont work because response is saved in cache
    }

    private function _tradShapefileField(string $key, int &$v = 0): string
    {
        $translationKey = "{$this->_translateKey}.{$key}";
        $translation = trans($translationKey);

        if ($translation === $translationKey) {
            // fallback if trans exports.{$key} not exists
            $translation = $key;
        }

        if ($v > 0) {
            $v_length = strlen((string) $v);
            return Str::of($translation)->substr(0, 9 - $v_length)->trim()->slug('_')->upper()->toString() . $v;
        }

        return Str::of($translation)->substr(0, 9)->trim()->slug('_')->upper()->toString();
    }

    private function _getShapefileKeyMapping(Collection $collection, DatakodeDataBag $dataClass): array
    {
        $res = [];
        $firstEntity = $collection->first();
        if (! $firstEntity) {
            return [];
        }

        $properties = Arr::dot($dataClass->from($firstEntity)->toArray());
        collect($properties)
            ->each(function ($item, $key) use (&$res): void {
                $v = 0;
                $shapefileKey = null;
                $alreadyExists = true;
                while (
                    $shapefileKey === null || $alreadyExists
                ) {
                    $shapefileKey = $this->_tradShapefileField($key, $v);
                    $alreadyExists = collect($res)
                        ->filter(function (array $i) use ($shapefileKey): bool {
                            return $i['shapefileKey'] === $shapefileKey;
                        })->count() > 0;
                    $v++;
                }

                $res[str($key)->slug()->toString()] = compact('shapefileKey');

            });

        return $res;
    }

    public function buildShapefile(
        Collection $collection,
        bool $flatten,
        array $keyMapping,
        ShapefileWriter $shapefileWriter,
        DatakodeDataBag $dataClass
    ): void {
        $chunkSize = 100;
        $nb = 0;
        $chunks = $collection->chunk($chunkSize);
        foreach ($chunks->all() as $chunk) {
            $chunk->each(function ($item) use ($flatten, $keyMapping, $shapefileWriter, $dataClass): void {
                /** @var Geometry|null $geometry */
                $geometry = $item->getShapefileGeometry();
                if ($geometry) { // remove item without geometry

                    $properties = $dataClass->from($item)
                        ->toArray();
                    if ($flatten) {
                        $properties = Arr::dot($properties);
                    }

                    collect($properties)
                        ->each(function ($item, $key) use ($keyMapping, $geometry): void {
                            $_key = Arr::get($keyMapping, str($key)->slug() . '.shapefileKey');
                            if ($_key) {
                                $geometry->setData($_key, (string) $item);
                            } else {
                                Log::warning('Unknown data key "' . $key . '" for shapefileKey mapping');
                            }
                        });
                    $shapefileWriter->writeRecord($geometry);
                }
            });

            $nb += $chunkSize;
            $this->progress($nb, 'retrieving data');
        }
    }
}
