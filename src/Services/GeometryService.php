<?php

namespace Datakode\LaravelDatakodeUtils\Services;

use Illuminate\Support\Facades\DB;
use MatanYadaev\EloquentSpatial\Objects\Geometry;
use MatanYadaev\EloquentSpatial\Objects\Polygon;
use RuntimeException;

class GeometryService
{
    public function __construct(
        public Geometry $geometry
    ) {
    }

    public function fillPolygonHoles(): self
    {
        $sql = "
        SELECT ST_AsText(ST_MakePolygon(
            ST_ExteriorRing(ST_GeomFromText(' {$this->geometry->toWkt()}'))
            )) as wkt
        ";
        $wkt = DB::select($sql)[0]->wkt;

        $this->geometry = Polygon::fromWkt($wkt, $this->geometry->srid);
        return $this;
    }

    public function buffer(float $distance_meters, int $quad_segs = 10): self
    {
        // @see https://postgis.net/docs/ST_Buffer.html
        // ::geography to set buffer in meters
        $sql = "
        SELECT ST_AsText(
            ST_Buffer(
            ST_GeomFromText(' {$this->geometry->toWkt()}')::geography
             ,{$distance_meters} , 'quad_segs={$quad_segs}')
            ) as wkt
        ";

        $wkt = DB::select($sql)[0]->wkt;

        $this->geometry = Geometry::fromWkt($wkt, $this->geometry->srid);
        return $this;
    }

    public function makeValid(): self
    {
        // @see https://postgis.net/docs/MakeValid.html
        $sql = "
        SELECT ST_MakeValid(
            ST_GeomFromText(' {$this->geometry->toWkt()}')
            , 'method=structure') as wkt
        ";

        $wkt = DB::select($sql)[0]->wkt;

        $this->geometry = Geometry::fromWkt($wkt, $this->geometry->srid);
        return $this;
    }

    public function getBBox(): array
    {
        $wkt = $this->geometry->toWkt();
        $srid = $this->geometry->srid;

        $sql = '
            SELECT ST_Extent(ST_GeomFromText(?, ?)) as bbox
        ';

        $result = DB::select($sql, [$wkt, $srid]);

        // Parse the BOX format returned by ST_Extent (e.g., BOX(x_min y_min, x_max y_max))
        if (isset($result[0]->bbox)) {
            preg_match('/BOX\(([-\d.]+) ([-\d.]+),([-\d.]+) ([-\d.]+)\)/', $result[0]->bbox, $matches);

            return [
                'x_min' => (float) $matches[1],
                'y_min' => (float) $matches[2],
                'x_max' => (float) $matches[3],
                'y_max' => (float) $matches[4],
            ];
        }

        throw new RuntimeException('Failed to calculate bounding box');
    }
}
