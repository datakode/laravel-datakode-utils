<?php

namespace Datakode\LaravelDatakodeUtils\QueryBuilder\Filters;

trait DatakodeFilterTrait
{
    public static function getType(): string
    {
        return class_basename(static::class);
    }
}
