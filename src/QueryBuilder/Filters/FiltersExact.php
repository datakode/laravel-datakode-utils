<?php

namespace Datakode\LaravelDatakodeUtils\QueryBuilder\Filters;

class FiltersExact extends \Spatie\QueryBuilder\Filters\FiltersExact
{
    use DatakodeFilterTrait;
}
