<?php

namespace Datakode\LaravelDatakodeUtils\QueryBuilder\Filters;

use Carbon\Carbon;
use Override;
use Spatie\QueryBuilder\Filters\Filter;

class FiltersDateTime implements Filter
{
    use DatakodeFilterWithOperators;

    #[Override]
    protected function formatValue($value): mixed
    {
        return new Carbon($value);
    }
}
