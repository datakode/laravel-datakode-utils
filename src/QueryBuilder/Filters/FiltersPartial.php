<?php

namespace Datakode\LaravelDatakodeUtils\QueryBuilder\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\Grammars\PostgresGrammar;
use Illuminate\Support\Str;
use Override;

class FiltersPartial extends FiltersPartialStrict
{
    #[Override]
    protected function getWhereRawParameters($value, string $property, Builder $query): array
    {
        $value = mb_strtolower((string) $value, 'UTF8');
        $value = Str::ascii($value); // remove search value accents

        $grammar = $query->getGrammar();
        if ($grammar::class === PostgresGrammar::class) {

            return ["unaccent({$property}) ILIKE ?", ['%' . $this->escapeLike($value) . '%']];
        }

        return ["LOWER({$property}) LIKE ?", ['%' . $this->escapeLike($value) . '%']];
    }
}
