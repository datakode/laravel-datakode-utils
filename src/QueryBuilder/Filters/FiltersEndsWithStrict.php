<?php

namespace Datakode\LaravelDatakodeUtils\QueryBuilder\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\Grammars\PostgresGrammar;
use Override;

class FiltersEndsWithStrict extends FiltersPartial
{
    #[Override]
    protected function getWhereRawParameters($value, string $property, Builder $query): array
    {
        $grammar = $query->getGrammar();
        if ($grammar::class === PostgresGrammar::class) {
            return ["{$property} ILIKE ?", ["%{$value}"]];
        }

        return ["{$property} LIKE ?", ["%{$value}"]];
    }
}
