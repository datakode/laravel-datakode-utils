<?php

namespace Datakode\LaravelDatakodeUtils\QueryBuilder\Filters;

class FiltersCallback extends \Spatie\QueryBuilder\Filters\FiltersCallback
{
    use DatakodeFilterTrait;
}
