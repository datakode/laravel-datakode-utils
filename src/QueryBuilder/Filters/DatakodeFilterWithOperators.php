<?php

namespace Datakode\LaravelDatakodeUtils\QueryBuilder\Filters;

use Illuminate\Database\Eloquent\Builder;
use RuntimeException;

trait DatakodeFilterWithOperators
{
    /**
     * @var string[]
     */
    public static array $availableOperators = ['eq', 'lt', 'gt', 'lte', 'gte'];

    public function __invoke(Builder $query, $value, string $property): void
    {
        if (! is_array($value)) {
            $query->where($property, $this->formatValue($value));

            return;
        }

        foreach ($value as $operator => $operatorValue) {
            $operatorValue = $this->formatValue($operatorValue);
            match ($operator) {
                'eq' => $query->where($query->qualifyColumn($property), '=', $operatorValue),
                'lt' => $query->where($query->qualifyColumn($property), '<', $operatorValue),
                'lte' => $query->where($query->qualifyColumn($property), '<=', $operatorValue),
                'gt' => $query->where($query->qualifyColumn($property), '>', $operatorValue),
                'gte' => $query->where($query->qualifyColumn($property), '>=', $operatorValue),
                default => throw new RuntimeException("Unhandled operator '{$operator}' for filter: " . self::class),
            };
        }
    }

    abstract protected function formatValue($value): mixed;
}
