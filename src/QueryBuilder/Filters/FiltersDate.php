<?php

namespace Datakode\LaravelDatakodeUtils\QueryBuilder\Filters;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Override;
use RuntimeException;
use Spatie\QueryBuilder\Filters\Filter;

class FiltersDate implements Filter
{
    use DatakodeFilterWithOperators;

    #[Override]
    public function __invoke(Builder $query, $value, string $property): void
    {
        if (! is_array($value)) {
            $value = $this->formatValue($value);
            $query->where($query->qualifyColumn($property), '>=', $value)
                ->where($query->qualifyColumn($property), '<', $value->clone()->endOfDay());

            return;
        }

        foreach ($value as $operator => $operatorValue) {
            /** @var Carbon $operatorValue */
            $operatorValue = $this->formatValue($operatorValue);
            match ($operator) {
                'eq' => $query->where($query->qualifyColumn($property), '>=', $operatorValue)
                    ->where($query->qualifyColumn($property), '<', $operatorValue->clone()->endOfDay()),
                'lt' => $query->where($query->qualifyColumn($property), '<', $operatorValue),
                'lte' => $query->where($query->qualifyColumn($property), '<=', $operatorValue),
                'gt' => $query->where($query->qualifyColumn($property), '>', $operatorValue),
                'gte' => $query->where($query->qualifyColumn($property), '>=', $operatorValue),
                default => throw new RuntimeException("Unhandled operator '{$operator}' for filter: " . self::class),
            };
        }
    }

    #[Override]
    protected function formatValue($value): mixed
    {
        return (new Carbon($value))->startOfDay();
    }
}
