<?php

namespace Datakode\LaravelDatakodeUtils\QueryBuilder\Filters;

use Illuminate\Contracts\Database\Query\Expression;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\Grammars\PostgresGrammar;
use Override;

class FiltersPartialStrict extends FiltersExact
{
    #[Override]
    public function __invoke(Builder $query, $value, string $property): void
    {
        if ($this->addRelationConstraint && $this->isRelationProperty($query, $property)) {
            $this->withRelationConstraint($query, $value, $property);

            return;
        }

        $wrappedProperty = $query->getQuery()
            ->getGrammar()
            ->wrap($query->qualifyColumn($property));

        if (is_array($value)) {
            $this->handleArray($value, $query, $wrappedProperty);

            return;
        }

        [$sql, $bindings] = $this->getWhereRawParameters($value, $wrappedProperty, $query);
        $query->whereRaw($sql, $bindings);
    }

    protected function getWhereRawParameters($value, string $property, Builder $query): array
    {
        $value = mb_strtolower((string) $value, 'UTF8');

        $grammar = $query->getGrammar();
        if ($grammar::class === PostgresGrammar::class) {
            return ["{$property} ILIKE ?", ['%' . $this->escapeLike($value) . '%']];
        }

        return ["LOWER({$property}) LIKE ?", ['%' . $this->escapeLike($value) . '%']];
    }

    protected function escapeLike(string $value): string
    {
        return str_replace(['\\', '_', '%'], ['\\\\', '\\_', '\\%'], $value);
    }

    protected function handleArray(array $value, Builder $query, float|int|string|Expression $wrappedProperty): Builder
    {
        $filteredArray = array_filter($value, function (string $part): bool {
            return strlen($part) > 0;
        });

        if ($filteredArray === []) {
            return $query;
        }

        $query->where(function (Builder $query) use ($filteredArray, $wrappedProperty): void {
            foreach ($filteredArray as $partialValue) {
                [$sql, $bindings] = $this->getWhereRawParameters($partialValue, $wrappedProperty, $query);
                $query->orWhereRaw($sql, $bindings);
            }
        });

        return $query;
    }
}
