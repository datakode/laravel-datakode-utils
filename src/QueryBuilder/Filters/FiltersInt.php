<?php

namespace Datakode\LaravelDatakodeUtils\QueryBuilder\Filters;

use Override;
use Spatie\QueryBuilder\Filters\Filter;

class FiltersInt implements Filter
{
    use DatakodeFilterWithOperators;

    #[Override]
    protected function formatValue($value): mixed
    {
        return (int) $value;
    }
}
