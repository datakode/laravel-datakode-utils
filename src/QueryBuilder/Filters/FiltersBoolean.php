<?php

namespace Datakode\LaravelDatakodeUtils\QueryBuilder\Filters;

use Illuminate\Database\Eloquent\Builder;
use Override;

class FiltersBoolean extends FiltersExact
{
    #[Override]
    public function __invoke(Builder $query, $value, string $property): void
    {
        if (! is_bool($value)) {
            return;
        }

        parent::__invoke($query, $value, $property);
    }
}
