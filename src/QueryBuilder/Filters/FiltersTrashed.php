<?php

namespace Datakode\LaravelDatakodeUtils\QueryBuilder\Filters;

class FiltersTrashed extends \Spatie\QueryBuilder\Filters\FiltersTrashed
{
    use DatakodeFilterTrait;
}
