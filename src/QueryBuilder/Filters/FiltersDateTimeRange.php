<?php

namespace Datakode\LaravelDatakodeUtils\QueryBuilder\Filters;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Override;
use Spatie\QueryBuilder\Filters\Filter;

class FiltersDateTimeRange implements Filter
{
    use DatakodeFilterWithOperators;

    #[Override]
    public function __invoke(Builder $query, mixed $value, string $property): void
    {
        [$min, $max] = $value;

        /** @var Carbon $minDate */
        $minDate = $this->formatValue($min);
        /** @var Carbon $maxDate */
        $maxDate = $this->formatValue($max);
        $query->where($query->qualifyColumn($property), '>=', $minDate)
            ->where($query->qualifyColumn($property), '<=', $maxDate);
    }

    #[Override]
    protected function formatValue($value): mixed
    {
        return (new Carbon($value))->startOfDay();
    }
}
