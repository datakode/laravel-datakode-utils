<?php

namespace Datakode\LaravelDatakodeUtils\QueryBuilder\Filters;

use Illuminate\Database\Eloquent\Builder;
use Override;

class FiltersExactOnlyIfType extends FiltersExact
{
    public function __construct(
        bool $addRelationConstraint,
        protected string $onlyIfType
    ) {
        parent::__construct($addRelationConstraint);
    }

    #[Override]
    public function __invoke(Builder $query, $value, string $property): void
    {
        switch ($this->onlyIfType) {
            case 'int':
                if (! is_int($value)) {
                    $query->whereIn($query->qualifyColumn($property), []);

                    return;
                }

                break;
            case 'bool':
                if (! is_bool($value)) {
                    $query->whereIn($query->qualifyColumn($property), []);

                    return;
                }

                break;
            case 'float':
                if (! is_float($value)) {
                    $query->whereIn($query->qualifyColumn($property), []);

                    return;
                }

                break;
        }

        parent::__invoke($query, $value, $property);
    }

    #[Override]
    public static function getType(): string
    {
        return class_basename(FiltersExact::class);
    }
}
