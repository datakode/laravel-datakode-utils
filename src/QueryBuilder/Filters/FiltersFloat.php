<?php

namespace Datakode\LaravelDatakodeUtils\QueryBuilder\Filters;

use Override;
use Spatie\QueryBuilder\Filters\Filter;

class FiltersFloat implements Filter
{
    use DatakodeFilterWithOperators;

    #[Override]
    protected function formatValue($value): mixed
    {
        return (float) $value;
    }
}
