<?php

/** @noinspection PhpUnused */

namespace Datakode\LaravelDatakodeUtils\QueryBuilder;

use Illuminate\Support\Str;
use Spatie\QueryBuilder\AllowedSort;

class DatakodeAllowedSort extends AllowedSort
{
    public function getDefaultDirection(): string
    {
        return $this->defaultDirection;
    }

    public function isActive(): bool
    {
        $request = request();
        $sortParam = trim((string) $request->get(config('query-builder.parameters.sort'), ''), '-');
        return $sortParam === $this->name;
    }

    public function activeDirection(): string|null
    {
        if (! $this->isActive()) {
            return null;
        }

        $request = request();
        $reverse = Str::substr($request->get(config('query-builder.parameters.sort')), 0, 1) === '-';
        if ($this->getDefaultDirection() === 'desc') {
            $reverse = -$reverse;
        }

        return $reverse ? 'desc' : 'asc';
    }

    /**
     * Sort using unaccent
     */
    public static function unaccentField(string $name, ?string $internalName = null): AllowedSort
    {
        return self::custom($name, new DatakodeUnaccentSortsField(), $internalName);
    }
}
