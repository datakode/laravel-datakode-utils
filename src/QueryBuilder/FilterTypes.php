<?php

namespace Datakode\LaravelDatakodeUtils\QueryBuilder;

enum FilterTypes
{
    case partial;
    case exact;
    case beginsWithStrict;
    case endsWithStrict;
    case scope;
    case trashed;
}
