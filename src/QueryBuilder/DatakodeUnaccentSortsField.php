<?php

namespace Datakode\LaravelDatakodeUtils\QueryBuilder;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\PostgresConnection;
use Illuminate\Support\Facades\DB;
use Override;
use Spatie\QueryBuilder\Sorts\SortsField;

/**
 * @see https://docs.postgresql.fr/13/unaccent.html
 */
class DatakodeUnaccentSortsField extends SortsField
{
    #[Override]
    public function __invoke(Builder $query, bool $descending, string $property): void
    {
        if (DB::connection() instanceof PostgresConnection) {
            $property = collect(explode('.', $property))
                ->map(function (string $item): string { return '"' . $item . '"'; })
                ->join('.');
            $query->orderByRaw('LOWER(unaccent(' . $property . ')) ' . ($descending ? 'DESC' : 'ASC'));
            return;
        }

        parent::__invoke($query, $descending, $property);
    }
}
