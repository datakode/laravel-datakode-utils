<?php

namespace Datakode\LaravelDatakodeUtils\QueryBuilder\Sorts;

use Illuminate\Database\Eloquent\Builder;
use Override;
use Spatie\QueryBuilder\Sorts\Sort;

class AggregateMaxSort implements Sort
{
    #[Override]
    public function __invoke(Builder $query, bool $descending, string $property): void
    {
        $direction = $descending ? 'DESC' : 'ASC';

        $query->orderByRaw("MAX({$property}) {$direction}");
    }
}
