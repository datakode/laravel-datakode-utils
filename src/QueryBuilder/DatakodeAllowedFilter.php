<?php

/** @noinspection PhpUnused */

namespace Datakode\LaravelDatakodeUtils\QueryBuilder;

use Carbon\Carbon;
use Closure;
use Datakode\LaravelDatakodeUtils\DatakodeAuth;
use Datakode\LaravelDatakodeUtils\LaravelData\ListData;
use Datakode\LaravelDatakodeUtils\Model\DatakodeApiModel;
use Datakode\LaravelDatakodeUtils\QueryBuilder\Filters\FiltersBeginsWithStrict;
use Datakode\LaravelDatakodeUtils\QueryBuilder\Filters\FiltersBoolean;
use Datakode\LaravelDatakodeUtils\QueryBuilder\Filters\FiltersCallback;
use Datakode\LaravelDatakodeUtils\QueryBuilder\Filters\FiltersDate;
use Datakode\LaravelDatakodeUtils\QueryBuilder\Filters\FiltersDateRange;
use Datakode\LaravelDatakodeUtils\QueryBuilder\Filters\FiltersDateTime;
use Datakode\LaravelDatakodeUtils\QueryBuilder\Filters\FiltersDateTimeRange;
use Datakode\LaravelDatakodeUtils\QueryBuilder\Filters\FiltersEndsWithStrict;
use Datakode\LaravelDatakodeUtils\QueryBuilder\Filters\FiltersExact;
use Datakode\LaravelDatakodeUtils\QueryBuilder\Filters\FiltersExactOnlyIfType;
use Datakode\LaravelDatakodeUtils\QueryBuilder\Filters\FiltersFloat;
use Datakode\LaravelDatakodeUtils\QueryBuilder\Filters\FiltersInt;
use Datakode\LaravelDatakodeUtils\QueryBuilder\Filters\FiltersPartial;
use Datakode\LaravelDatakodeUtils\QueryBuilder\Filters\FiltersPartialStrict;
use Datakode\LaravelDatakodeUtils\QueryBuilder\Filters\FiltersTrashed;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Override;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\Filters\Filter;
use Spatie\QueryBuilder\Filters\FiltersScope;
use Spatie\QueryBuilder\QueryBuilder;

class DatakodeAllowedFilter extends AllowedFilter
{
    public float|Carbon|null $min = null;

    public float|Carbon|null $max = null;

    public ?float $step = null;

    /**
     * @var string[]|null
     */
    public ?array $availableOperators = null;

    private ?array $_availableValues = null;

    private ?string $_info = null;

    private ?string $_autocompleteRoute = null;

    private array $_autocompleteRouteFilter = [];

    private ?bool $multiple = null;

    private mixed $_initialValue = null;

    private ?string $_autocompleteEntityModel = null;

    private ?Closure $_customFilterQuery = null;

    public bool $hidden = false;

    /**
     * Filter by softdelete status
     */
    public static function hasTrashed(bool $default = false): self
    {
        return self::scope('has_trashed')->setAvailableValues(['true', 'false', 'all'])->default($default);
    }

    /**
     * List available datas
     *
     * @return $this
     */
    public function setAvailableValues(?array $availableValues): self
    {
        $this->_availableValues = $availableValues;

        return $this;
    }

    /**
     * @param string|null $autocompleteRoute route name
     * @param array $filter Filters params to had to query url
     * @param string|null $ifHasPermission Remove filter if user doesnt have a permission
     * @return DatakodeAllowedFilter|$this|null
     */
    public function setAutocompleteRoute(
        ?string $autocompleteRoute,
        array $filter = [],
        ?string $ifHasPermission = null,
        ?string $entityModel = null,
    ): ?self {
        if ($ifHasPermission !== null) {
            $user = DatakodeAuth::currentUser();
            if (! $user?->hasPermissionString($ifHasPermission)) {
                return null;
            }
        }

        $this->_autocompleteRoute = $autocompleteRoute;
        $this->_autocompleteRouteFilter = $filter;
        $this->_autocompleteEntityModel = $entityModel;

        return $this;
    }

    /**
     * Remove filter if user doesnt have a permission
     *
     * @return DatakodeAllowedFilter|$this|null
     */
    public function onlyIfHasPermission(?string $ifHasPermission = null): ?self
    {
        $user = DatakodeAuth::currentUser();
        if (! $user?->hasPermissionString($ifHasPermission)) {
            return null;
        }

        return $this;
    }

    /**
     * Add info data in meta
     *
     * @return $this
     */
    public function setInfo(?string $info): self
    {
        $this->_info = $info;

        return $this;
    }

    public function getType(): string
    {
        $className = $this->filterClass;
        if (method_exists($className, 'getType')) {
            /** @var FiltersExact $className */
            return $className::getType();
        }

        return class_basename($this->filterClass::class);
    }

    public function getAvailableValues(): ?array
    {
        return $this->_availableValues;
    }

    public function getInfo(): ?string
    {
        return $this->_info;
    }

    public function getAutocompleteUrl(): ?string
    {
        $routeParameters = [
            'filter' => $this->_autocompleteRouteFilter,
            'format' => 'list',
        ];
        if (Route::has($this->_autocompleteRoute)) {
            return Str::of(route($this->_autocompleteRoute, $routeParameters))->replaceStart(
                route('api.home') . '/',
                ''
            );
        }

        if (Route::has($this->_autocompleteRoute . '.index')) {
            return Str::of(route($this->_autocompleteRoute . '.index', $routeParameters))->replaceStart(
                route('api.home') . '/',
                ''
            );
        }

        return $this->_autocompleteRoute;
    }

    /* =======
     * FILTERS
       ======= */

    /**
     * @param string|null $onlyIfType available values 'int'|'bool'|'float'
     */
    #[Override]
    public static function exact(
        string $name,
        ?string $internalName = null,
        bool $addRelationConstraint = true,
        ?string $arrayValueDelimiter = null,
        ?bool $multiple = true,
        ?string $onlyIfType = null
    ): self {
        static::setFilterArrayValueDelimiter($arrayValueDelimiter);

        $res = $onlyIfType !== null ?
            new self($name, new FiltersExactOnlyIfType($addRelationConstraint, $onlyIfType), $internalName)
            : new self($name, new FiltersExact($addRelationConstraint), $internalName);

        $res->multiple = $multiple;
        return $res;
    }

    #[Override]
    public static function partial(
        string $name,
        $internalName = null,
        bool $addRelationConstraint = true,
        ?string $arrayValueDelimiter = null
    ): self {
        static::setFilterArrayValueDelimiter($arrayValueDelimiter);

        return new self($name, new FiltersPartial($addRelationConstraint), $internalName);
    }

    /**
     * Partial with strict accent check
     */
    public static function partialStrict(
        string $name,
        $internalName = null,
        bool $addRelationConstraint = true,
        ?string $arrayValueDelimiter = null
    ): self {
        static::setFilterArrayValueDelimiter($arrayValueDelimiter);

        return new self($name, new FiltersPartialStrict($addRelationConstraint), $internalName);
    }

    #[Override]
    public static function beginsWithStrict(
        string $name,
        $internalName = null,
        bool $addRelationConstraint = true,
        ?string $arrayValueDelimiter = null
    ): self {
        static::setFilterArrayValueDelimiter($arrayValueDelimiter);

        return new self($name, new FiltersBeginsWithStrict($addRelationConstraint), $internalName);
    }

    #[Override]
    public static function endsWithStrict(
        string $name,
        $internalName = null,
        bool $addRelationConstraint = true,
        ?string $arrayValueDelimiter = null
    ): self {
        static::setFilterArrayValueDelimiter($arrayValueDelimiter);

        return new self($name, new FiltersEndsWithStrict($addRelationConstraint), $internalName);
    }

    #[Override]
    public static function scope(
        string $name,
        $internalName = null,
        ?string $arrayValueDelimiter = null,
        ?bool $multiple = true,
    ): self {
        static::setFilterArrayValueDelimiter($arrayValueDelimiter);

        $res = new self($name, new FiltersScope(), $internalName);
        $res->multiple = $multiple;
        return $res;
    }

    public static function boolean(
        string $name,
        $internalName = null,
        ?string $arrayValueDelimiter = null,
        bool $displayAllChoice = true
    ): self {
        static::setFilterArrayValueDelimiter($arrayValueDelimiter);

        $res = (new self($name, new FiltersBoolean(), $internalName))
            ->setAvailableValues(['true', 'false', ...$displayAllChoice ? ['all'] : []]);

        $res->multiple = false;
        return $res;
    }

    #[Override]
    public static function callback(
        string $name,
        $callback,
        $internalName = null,
        ?string $arrayValueDelimiter = null,
        ?bool $multiple = true,
    ): self {
        self::setFilterArrayValueDelimiter($arrayValueDelimiter);

        $res = new self($name, new FiltersCallback($callback), $internalName);
        $res->multiple = $multiple;
        return $res;
    }

    #[Override]
    public static function trashed(string $name = 'trashed', $internalName = null): self
    {
        return new self($name, new FiltersTrashed(), $internalName);
    }

    #[Override]
    public static function custom(
        string $name,
        Filter $filterClass,
        $internalName = null,
        ?string $arrayValueDelimiter = null,
        ?bool $multiple = true,
    ): self {
        self::setFilterArrayValueDelimiter($arrayValueDelimiter);

        $res = new self($name, $filterClass, $internalName);
        $res->multiple = $multiple;
        return $res;
    }

    public static function float(
        string $name,
        $internalName = null,
        ?string $arrayValueDelimiter = null,
        ?float $min = null,
        ?float $max = null,
        float $step = 1
    ): self {
        static::setFilterArrayValueDelimiter($arrayValueDelimiter);

        $res = new self($name, new FiltersFloat(), $internalName);
        $res->min = $min;
        $res->max = $max;
        $res->step = $step;
        $res->availableOperators = FiltersFloat::$availableOperators;

        return $res;
    }

    public static function int(
        string $name,
        $internalName = null,
        ?string $arrayValueDelimiter = null,
        ?float $min = null,
        ?float $max = null,
        float $step = 1
    ): self {
        static::setFilterArrayValueDelimiter($arrayValueDelimiter);

        $res = new self($name, new FiltersInt(), $internalName);
        $res->min = $min;
        $res->max = $max;
        $res->step = $step;
        $res->availableOperators = FiltersInt::$availableOperators;

        return $res;
    }

    public static function date(
        string $name,
        $internalName = null,
        ?string $arrayValueDelimiter = null,
        ?Carbon $min = null,
        ?Carbon $max = null
    ): self {
        static::setFilterArrayValueDelimiter($arrayValueDelimiter);

        $res = new self($name, new FiltersDate(), $internalName);
        $res->min = $min;
        $res->max = $max;
        $res->availableOperators = FiltersDate::$availableOperators;

        return $res;
    }

    public static function dateRange(
        string $name,
        $internalName = null,
        ?string $arrayValueDelimiter = null,
        ?Carbon $min = null,
        ?Carbon $max = null
    ): self {
        static::setFilterArrayValueDelimiter($arrayValueDelimiter);

        $res = new self($name, new FiltersDateRange(), $internalName);
        $res->min = $min;
        $res->max = $max;

        return $res;
    }

    public static function datetime(
        string $name,
        $internalName = null,
        ?string $arrayValueDelimiter = null,
        ?Carbon $min = null,
        ?Carbon $max = null
    ): self {
        static::setFilterArrayValueDelimiter($arrayValueDelimiter);

        $res = new self($name, new FiltersDateTime(), $internalName);
        $res->min = $min;
        $res->max = $max;
        $res->availableOperators = FiltersDateTime::$availableOperators;

        return $res;
    }

    public static function dateTimeRange(
        string $name,
        $internalName = null,
        ?string $arrayValueDelimiter = null,
        ?Carbon $min = null,
        ?Carbon $max = null
    ): self {
        static::setFilterArrayValueDelimiter($arrayValueDelimiter);

        $res = new self($name, new FiltersDateTimeRange(), $internalName);
        $res->min = $min;
        $res->max = $max;

        return $res;
    }

    // END filters

    #[Override]
    public function default($value): self
    {
        /** @var DatakodeAllowedFilter $res */
        $res = parent::default($value);

        return $res;
    }

    public function initialValue(mixed $value): self
    {
        $this->_initialValue = $value;

        return $this;
    }

    public function getMeta(): array
    {
        return [
            'type' => $this->getType(),
            'name' => $this->getName(),

            ...$this->getInternalName() !== $this->getName() ? [
                'internal_name' => $this->getInternalName(),
            ] : [],

            'ignored' => $this->getIgnored(),
            'default' => $this->getDefault(),

            ...$this->_ifNotNull('initial_value', $this->_initialValue),
            ...$this->_ifNotNull('available_values', $this->getAvailableValues()),
            ...$this->_ifNotNull('autocomplete_url', $this->getAutocompleteUrl()),
            ...$this->_ifNotNull('availableOperators', $this->availableOperators),
            ...$this->_ifNotNull('min', $this->min),
            ...$this->_ifNotNull('max', $this->max),
            ...$this->_ifNotNull('step', $this->step),
            ...$this->_ifNotNull('multiple', $this->multiple),
            ...$this->_ifNotNull('info', $this->getInfo()),

            'is_active' => $this->isActive(),
            ...$this->_ifNotNull('active_values', $this->getActiveFilterValues()),
        ];
    }

    private function _ifNotNull(string $key, mixed $value): array
    {
        if ($value === null) {
            return [];
        }

        return [
            $key => $value,
        ];
    }

    protected function isActive(): bool
    {
        $request = request();
        $filterParam = $request->get(config('query-builder.parameters.filter'));
        return $filterParam && in_array($this->name, array_keys($filterParam));
    }

    protected function getActiveFilterValues(): array|null
    {
        if (! $this->isActive()) {
            return null;
        }

        $request = request();
        $filterParam = $request->get(config('query-builder.parameters.filter'));
        if (! $this->multiple) {
            return [$this->_getFilterCurrentMeta($filterParam[$this->name])];
        }

        if (! is_array($filterParam[$this->name])) {
            $filterParam[$this->name] = explode(',', (string) $filterParam[$this->name]);
        }

        return collect($filterParam[$this->name])->map(
            fn (string $value): string|array => $this->_getFilterCurrentMeta($value)
        )->toArray();
    }

    public function _getFilterCurrentMeta(string|array $filterParam): string|array
    {
        if (! $this->_autocompleteEntityModel) {
            return $filterParam;
        }

        /** @var DatakodeApiModel $model */
        $model = $this->_autocompleteEntityModel;
        $filterEntity = $model::query()->where((new $model())->getRouteKeyName(), $filterParam)->first();
        if (! $filterEntity) {
            return $filterParam;
        }

        return ListData::from($filterEntity)->toArray();
    }

    public function hidden(): static
    {
        $this->hidden = true;
        return $this;
    }

    public function customQueryFilter(callable $closure): static
    {
        $this->_customFilterQuery = $closure;
        return $this;
    }

    #[Override]
    public function filter(QueryBuilder $query, $value)
    {
        if ($this->_customFilterQuery) {
            $func = $this->_customFilterQuery;
            return $func($query, $value);
        }

        return parent::filter($query, $value);
    }
}
