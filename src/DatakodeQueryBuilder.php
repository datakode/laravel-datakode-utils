<?php

/** @noinspection PhpUnused */

namespace Datakode\LaravelDatakodeUtils;

use Carbon\Carbon;
use Closure;
use Datakode\LaravelDatakodeUtils\LaravelData\DatakodeData;
use Datakode\LaravelDatakodeUtils\LaravelData\DatakodeDataBag;
use Datakode\LaravelDatakodeUtils\LaravelData\ListData;
use Datakode\LaravelDatakodeUtils\LaravelData\ProcessingData;
use Datakode\LaravelDatakodeUtils\Model\DatakodeApiModel;
use Datakode\LaravelDatakodeUtils\Model\Processing;
use Datakode\LaravelDatakodeUtils\QueryBuilder\DatakodeAllowedFilter;
use Datakode\LaravelDatakodeUtils\QueryBuilder\DatakodeAllowedSort;
use Illuminate\Contracts\Pagination\CursorPaginator;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\HigherOrderBuilderProxy;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Pagination\AbstractCursorPaginator;
use Illuminate\Pagination\AbstractPaginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Routing\Route;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Enumerable;
use Illuminate\Support\Facades\App;
use Illuminate\Support\LazyCollection;
use Override;
use RuntimeException;
use Spatie\LaravelData\CursorPaginatedDataCollection;
use Spatie\LaravelData\Data;
use Spatie\LaravelData\DataCollection;
use Spatie\LaravelData\PaginatedDataCollection;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder as BaseQueryBuilder;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

/**
 * @method jsonPaginate(int $maxResults = null, int $defaultSize = null)
 */
class DatakodeQueryBuilder extends BaseQueryBuilder
{
    /**
     * @var Builder|HigherOrderBuilderProxy|Relation|mixed|string
     */
    public static $model;

    public bool $allowExport = true;

    private array $_exportFormats = ['json', 'csv', 'geojson', 'shape', 'xlsx', 'ods', 'xls',
        //        , 'html', 'pdf'
    ];

    private bool $_allowedExports = false;

    private bool $_allowedGeojsonExports = false;

    private bool $_allowShapefileExport = false;

    private bool $_allowedEmptyDataResource = false;

    private null|string|array|DatakodeDataBag $_dataResourceClass = null;

    private null|string|array|DatakodeDataBag $_geojsonExportDataResourceClass = null;

    private null|string|array|DatakodeDataBag $_geojsonFormatDataResourceClass = null;

    private null|string|array|DatakodeDataBag $_exportDataResourceClass = null;

    private array $_customFormat = [];

    private array $_getters = [];

    private mixed $_customExport = [];

    private mixed $_customMeta = [];

    private bool $_useCache = false;

    private ?string $_subCacheKey = null;

    private ?string $_exportFileName = null;

    private bool $_flattenResult = false;

    protected static bool $disableOrderById = false;

    /**
     * @var array|null[]|string[]
     */
    private array $_exportTranslateKey = [];

    private bool $_flattenGeojsonResult = false;

    private bool $_flattenShapeResult = false;

    public bool $_queryDisableOrderById = false;

    /**
     * @var Closure|null
     */
    private mixed $_customDataTransform = null;

    public static function forModel(
        Model|string $modelClass,
        Builder $subject,
        ?Request $request = null,
        $allowExport = true
    ): static {
        static::$model = $modelClass;

        $searchNeedle = ($request ?? request())
            ->get('search');
        if ($searchNeedle) {
            /** @var DatakodeApiModel $sampleObject */
            $sampleObject = new $modelClass();
            if (count($sampleObject->searchable)) {
                /** @var DatakodeApiModel $subject */
                $subject->search($searchNeedle);
            } else {
                abort(HttpResponse::HTTP_BAD_REQUEST, 'Search is not available on this resource');
            }
        }

        $res = self::for($subject, $request);
        $res->allowExport = $allowExport;

        return $res;
    }

    #[Override]
    public static function for($subject, ?Request $request = null): static
    {
        $res = parent::for($subject, $request);
        $res->allowedSorts = collect();
        $res->allowedFilters = collect();
        return $res;
    }

    /**
     * This function will call a model getter for each model in a collection.
     * The model needs to have a method called getRecipientsWithParameters
     * if the getter name is 'recipients' for example
     * of getAttributeWithParameters for a getter named 'attribute'
     * One parameter can be passed to the getter function.
     *
     * @return $this
     */
    public function addGetter($name, $parameter): static
    {
        $this->_getters[$name] = $parameter;

        return $this;
    }

    /**
     * allow to export data
     *
     * @param string|array|DatakodeDataBag|null $exportDataResourceClass dataresource for export if null use main one
     * @param bool $allowExport on/off
     * @return $this
     */
    public function allowExports(
        null|string|array|DatakodeDataBag $exportDataResourceClass = null,
        bool $allowExport = true,
        string|null $translate_key_csv = 'exports',
        string|null $translate_key_ods = 'exports',
        string|null $translate_key_xls = 'exports',
        string|null $translate_key_xlsx = 'exports',
        string|null $translate_key_json = null,
        string|null $translate_key_geojson = null,
        string|null $translate_key_shape = 'exports.shape',
    ): static {
        $this->_exportDataResourceClass = $exportDataResourceClass;
        $this->_allowedExports = $allowExport;
        $this->_exportTranslateKey = [
            'csv' => $translate_key_csv,
            'ods' => $translate_key_ods,
            'xls' => $translate_key_xls,
            'xlsx' => $translate_key_xlsx,
            'json' => $translate_key_json,
            'geojson' => $translate_key_geojson,
            'shape' => $translate_key_shape,
        ];

        return $this;
    }

    /**
     * allow to export data as geojson
     *
     * @param bool $allowGeojsonExport on/off
     * @param bool $allowShapefileExport on/off
     * @return $this
     * @deprecated please use {@link allowGisExports}
     */
    public function allowGeojsonExports(bool $allowGeojsonExport = true, bool $allowShapefileExport = false): static
    {
        return $this->allowGisExports($allowGeojsonExport, $allowShapefileExport);
    }

    /**
     * allow to export data as GIS
     *
     * @param bool $allowGeojsonExport on/off
     * @param bool $allowShapefileExport on/off
     * @return $this
     */
    public function allowGisExports(
        bool $allowGeojsonExport = true,
        bool $allowShapefileExport = true,
        bool $flattenGeojsonResult = false,
        bool $flattenShapeResult = true,
    ): static {
        $this->_allowedGeojsonExports = $allowGeojsonExport;
        $this->_allowShapefileExport = $allowShapefileExport;
        $this->_flattenGeojsonResult = $flattenGeojsonResult;
        $this->_flattenShapeResult = $flattenShapeResult;

        return $this;
    }

    /**
     * extend \Spatie\QueryBuilder\Concerns\FiltersQuery::allowedFilters
     */
    #[Override]
    public function allowedFilters($filters): static
    {
        /** @var Collection $filtersCollection */
        $filtersCollection = collect($filters)
            ->filter(fn ($v): bool => $v !== null)
            ->map(function ($filter) {
                // force string filter to be DatakodeAllowedFilter object
                return is_string($filter) ? DatakodeAllowedFilter::partial($filter) : $filter;
            });

        $filtersCollection->push(
            DatakodeAllowedFilter::exact('k', $this->getModel()->getRouteKeyName())->hidden()
        );

        return parent::allowedFilters($filtersCollection->toArray());
    }

    /**
     * extend \Spatie\QueryBuilder\Concerns\SortsQuery::allowedSorts
     *
     * @return $this
     */
    #[Override]
    public function allowedSorts($sorts): static
    {
        $sorts = is_array($sorts) ? $sorts : func_get_args();

        $this->allowedSorts = collect($sorts)
            ->map(function ($sort): AllowedSort {
                if ($sort instanceof DatakodeAllowedSort) {
                    return $sort;
                }

                return DatakodeAllowedSort::field(ltrim($sort, '-'));
            });

        $this->ensureAllSortsExist();

        $this->addRequestedSortsToQuery(); // allowed is known & request is known, add what we can, if there is no request, -wait

        return $this;
    }

    /**
     * allow to use query builder without data ressource
     *
     * @return $this
     */
    public function allowEmptyDataResource(bool $value = true): static
    {
        $this->_allowedEmptyDataResource = $value;

        return $this;
    }

    /**
     * define data ressource class
     * must be un child class of \DatakodeUtils\DatakodeData
     * ex: \DatakodeUtils\DatakodeData::class
     *
     * @return $this
     */
    public function setDataResource(null|string|array|DatakodeDataBag $class): static
    {
        $this->_dataResourceClass = $class;

        return $this;
    }

    /**
     * @deprecated please use {@link setGisDataResource}
     */
    public function setGeojsonDataResource(null|string|array|DatakodeDataBag $class): static
    {
        return $this->setGisDataResource($class);
    }

    public function setGisDataResource(
        null|string|array|DatakodeDataBag $class = null,
        null|string|array|DatakodeDataBag $classForExports = null
    ): static {
        $this->_geojsonFormatDataResourceClass = $class;
        $this->_geojsonExportDataResourceClass = $classForExports ?? $class;

        return $this;
    }

    /**
     * get final collection
     */
    public function getCollection(string $format = null): Response|BinaryFileResponse
    {
        $request = request();
        if (! $this->_useCache || $request->input('export')) {
            return $this->_getCollectionOrExport($format);
        }

        $cache_key = $this->getCacheKey();

        return cache()->rememberForever($cache_key, function () use ($format): Response|BinaryFileResponse {
            return $this->_getCollectionOrExport($format);
        });
    }

    public function export(string $format): Response|BinaryFileResponse
    {
        $count = $this->toBase()
            ->getCountForPagination();
        if ($count === 0) {
            App::abort(HttpResponse::HTTP_BAD_REQUEST, 'Aucune données à exporter');
        }

        $maxExport = config('laravel-datakode-utils.exports.max');
        if ($maxExport && $count > $maxExport) {
            App::abort(
                HttpResponse::HTTP_BAD_REQUEST,
                "L'export est limité à {$maxExport} éléments. " . $count . ' éléments demandés.'
            );
        }

        $translateKey = Arr::get($this->_exportTranslateKey, $format, 'exports');
        $flatten = $this->_flattenResult
            || ($format === 'geojson' && $this->_flattenGeojsonResult)
            || ($format === 'shape' && $this->_flattenShapeResult);

        if (in_array($format, ['geojson', 'shape'])) {
            $this->_exportDataResourceClass = $this->_geojsonExportDataResourceClass;
        }

        if (in_array($format, $this->_exportFormats, true)) {
            return $this->_handleProcessing(Processing::init(
                $this,
                DatakodeConfig::configOrFail('laravel-datakode-utils.jobs.ProcessExport'),
                compact('format', 'translateKey', 'flatten')
            ));
        }

        if (Arr::has($this->_customExport, $format)) {
            $dataClass = Arr::get($this->_customExport, $format . '.dataClass');

            return $this->_handleProcessing(Processing::init(
                $this,
                Arr::get($this->_customExport, $format . '.exportClass'),
                compact('format', 'dataClass', 'translateKey', 'flatten')
            ));
        }

        abort(HttpResponse::HTTP_BAD_REQUEST, 'Unhandled export format ' . $format);

    }

    public function formatResult(array $array): array
    {
        if (Arr::has($array, 'data')) {
            $data = $array['data'];
        } else {
            $data = $array;
            $array = [];
        }

        if ($this->_flattenResult) {
            $data = collect($data)
                ->flatten()
                ->toArray();
        }

        $meta = $this->getMeta($array);
        ksort($meta);

        return compact('data', 'meta');
    }

    public function addCustomExport(string $format, string $exportClass, string $dataClass): self
    {
        $this->_customExport[$format] = compact('exportClass', 'dataClass');

        return $this;
    }

    public function useCache(bool $value = true, string $subCacheKey = null): static
    {
        $this->_useCache = $value;
        $this->_subCacheKey = $subCacheKey;

        return $this;
    }

    public function getCacheKey(): string
    {
        $request = request();

        $user = DatakodeAuth::currentUser();

        $keys = [
            $this->toSql(),
            json_encode($user?->getDatakodeQueryBuilderCacheKey()),
            json_encode($request->all()),
            json_encode([
                // add data transform parameters
                'flattenResult' => $this->_flattenResult,
            ]),
        ];

        if ($this->_subCacheKey) {
            $keys[] = $this->_subCacheKey;
        }

        return collect($keys)->join('|');
    }

    public function getExportFilename(): string
    {
        if ($this->_exportFileName) {
            return $this->_exportFileName;
        }

        $request = request();

        /** @var Route|null $route */
        $route = $request->route();
        $routeName = $route ? $route->getName() : '';
        return explode('.', (string) $routeName)[0];
    }

    public function getExportDataClass(): DatakodeDataBag
    {
        return new DatakodeDataBag($this->_exportDataResourceClass ?? $this->_dataResourceClass ?? ListData::class);
    }

    public function addFormat(string $name, Closure $method): static
    {
        $this->_customFormat[$name] = $method;

        return $this;
    }

    public function _handleFormatGeojson(): Response
    {
        if (! $this->_canExportGeom()) {
            abort(HttpResponse::HTTP_BAD_REQUEST, 'Resource can not been formatted as Geojson.');
        }

        return $this->_formatGeojson();
    }

    public function _handleFormatList(): Response
    {
        /** @var DatakodeApiModel $model */
        $model = $this->getModel();
        foreach ($model->getDefaultOrderBy() as $f) {
            $this->orderBy($f);
        }

        $data = $this->jsonPaginate();
        $collection = ListData::collect($data);

        return response($this->formatResult($collection->toArray()));
    }

    #[Override]
    protected function addRequestedSortsToQuery()
    {
        parent::addRequestedSortsToQuery();
        $this->_forceSortById();
    }

    public function disableOrderById(bool $disabled = true): self
    {
        $this->_queryDisableOrderById = $disabled;
        return $this;
    }

    /**
     * // FORCE sort by id (in order to get always same result order)
     */
    private function _forceSortById(): void
    {
        if (static::$disableOrderById || $this->_queryDisableOrderById) {
            return;
        }

        $model = $this->getModel();
        $this->orderBy($model->getTable() . '.' . $model->getKeyName());
    }

    public function _handleFormatPosition(): Response
    {
        // /** @var DatakodeApiModel $model */
        //        $model = $this->getQuery();
        //        foreach ($model->getDefaultOrderBy() as $f) {
        //            $this->orderBy($f);
        //        }
        $data = $this->get();

        $collection = $data->map(function (Model $i): string {
            /** @var DatakodeApiModel $datakodeModel */
            $datakodeModel = $i;
            return (string) $datakodeModel->getRouteKey();
        });

        return response([
            'data' => $collection->toArray(),
        ]);
    }

    public function _handleCustomFormat(string $format): Response
    {
        $method = Arr::get($this->_customFormat, $format);
        if ($method === null) {
            abort(HttpResponse::HTTP_BAD_REQUEST, 'Unhandled format: ' . $format);
        }

        return $method($this);
    }

    public static function geojsonResponse(
        Collection $data,
        string $filename = 'data.geojson',
        null|string|array|Data|DatakodeDataBag $dataClass = ListData::class,
        bool $flatten = false,
        string|null $translateKey = null,
    ): Response {
        $features = [];
        $type = 'Feature';
        $data->each(function ($item) use ($translateKey, $flatten, &$features, $type, $dataClass): void {
            $geometry = $item->getGeometryArray();
            if ($geometry) { // remove item without geometry
                $properties = DatakodeQueryBuilder::getData($dataClass, $item)
                    ->toArray();
                if ($flatten) {
                    $properties = collect($properties)
                        ->dot()
                        ->toArray();
                }

                if ($translateKey !== null) {
                    $properties = self::_translateKey($properties, $translateKey);

                }

                $features[] = compact('properties', 'type', 'geometry');
            }
        });

        $type = 'FeatureCollection';

        return (new DatakodeApiResponse(compact('type', 'features')))
            ->download('application/geo+json', $filename);
    }

    /**
     * Add data in meta
     */
    public function getMeta(array $data): array
    {
        $meta = [
            ...Arr::get($data, 'meta', []),
            ...collect($data)
                ->except(['data', 'links', 'meta'])->toArray(),
            ...$this->_customMeta,
        ];

        $request = request();

        $meta = [
            ...$meta,
            'request' => $request->only([
                ...array_values(config('query-builder.parameters')),
                config('json-api-paginate.cursor_parameter'),
                config('json-api-paginate.pagination_parameter'),
                config('json-api-paginate.number_parameter'),
                config('json-api-paginate.number_parameter'),
                'search', 'export', 'format',
            ]),
            'allowed_sorts' => $this->allowedSorts
                ->map(fn (DatakodeAllowedSort $sort): array => [
                    'name' => $sort->getName(),
                    'default_direction' => $sort->getDefaultDirection(),
                    'is_active' => $sort->isActive(),
                    ...($sort->isActive() ? [
                        'active_direction' => $sort->activeDirection(),
                    ] : []),
                ])->sortBy('name')
                ->values(),
            'allowed_filters' => $this->allowedFilters
                ->filter(fn (DatakodeAllowedFilter $filter): bool => ! $filter->hidden)
                ->map(fn (DatakodeAllowedFilter $filter): array => $filter->getMeta())
                ->values(),
            'searchable' => $this->_isSearchable(),
            'available_exports' => $this->_getAvailableExports(),
            'available_formats' => $this->_getAvailableFormats(),
        ];

        if (defined('LARAVEL_START')) {
            $meta['render_time_ms'] = round((microtime(true) - constant('LARAVEL_START')) * 1000);
        }

        $meta['time'] = Carbon::now()->toJSON();

        return $meta;
    }

    public function addMeta(string $key, mixed $value): static
    {
        $this->_customMeta[$key] = $value;
        return $this;
    }

    public function setExportFilename(string $string): static
    {
        $this->_exportFileName = $string;
        return $this;
    }

    public function flattenResult(bool $flatten = true): static
    {
        $this->_flattenResult = $flatten;
        return $this;
    }

    private function _getCollectionOrExport(string $format = null): Response|BinaryFileResponse
    {
        $request = request();
        if ($request->input('export')) {
            return $this->_returnExport($request);
        }

        if ($format || $request->has('format')) {
            // handle formats
            return $this->_returnFormatResult($request, $format);
        }

        $data = $this->jsonPaginate();

        $data = $data->through(function ($item) {
            foreach ($this->_getters as $getter => $params) {
                $item->{$getter} = $item->{'get' . ucfirst($getter) . 'WithParameters'}($params);
            }

            return $item;
        });

        if ($this->_customDataTransform) {
            $items = collect($data->items())->map($this->_customDataTransform);

            $data->setCollection($items);
            //            WIP change paginator collection items
            $array = $this->_getCollection($data);

            //            WIP
            return response($this->formatResult($array));
        }

        $array = $this->_getCollection($data);

        if (! $this->_dataResourceClass && ! $this->_allowedEmptyDataResource) {
            throw new RuntimeException('DataResource required ! Please use setDataResource(MyDataResource::class)');
        }

        return response($this->formatResult($array));
    }

    private function _getCollection(
        LengthAwarePaginator $data,
        array|string|null $exportDataResourceClass = null
    ): array {
        $dataClass = $exportDataResourceClass ?? $this->_dataResourceClass;

        $res = self::getData($dataClass, $data)
            ->toArray();

        if ($this->_flattenResult) {
            $res['data'] = collect($res['data'])->map(function ($line) {
                return collect($line)->dot()
                    ->toArray();
            })->toArray();
        }

        return $res;
    }

    protected static function getData(
        array|string|null|DatakodeDataBag $dataClass,
        LengthAwarePaginator|Collection|DatakodeApiModel $collection
    ): DatakodeData|CursorPaginator|Paginator|AbstractCursorPaginator|AbstractPaginator|Collection|Enumerable|LazyCollection|CursorPaginatedDataCollection|DataCollection|PaginatedDataCollection {
        if (! $dataClass) {
            return $collection;
        }

        return (new DatakodeDataBag($dataClass))->getData($collection);
    }

    public function geojson(
        Collection $data,
        string $filename,
        null|string|array|DatakodeDataBag $useDataClass = null
    ): Response {
        /** @var null|string|array|Data|DatakodeDataBag $dataClass */
        $dataClass = $useDataClass ?? $this->_exportDataResourceClass ?? $this->_dataResourceClass ?? ListData::class;

        return self::geojsonResponse(
            data: $data,
            filename: $filename,
            dataClass: $dataClass,
            flatten: $this->_flattenResult && $this->isExport(),
            translateKey: $this->isExport() ? Arr::get($this->_exportTranslateKey, 'geojson') : null
        );
    }

    private function _canExportGeom(): bool
    {
        return method_exists(self::$model, 'getGeometryArray');
    }

    private function _isSearchable(): bool
    {
        /** @var DatakodeApiModel $obj */
        $obj = new self::$model();

        return count($obj->searchable) > 0;
    }

    private function _getAvailableExports(): array
    {
        if (! $this->allowExport
            || ! $this->_allowedExports
        ) {
            return [];
        }

        $res = collect($this->_exportFormats);

        if (! $this->_allowedGeojsonExports || ! $this->_canExportGeom()) {
            $res = $res->filter(fn ($k): bool => $k != 'geojson')
                ->values();
        }

        if (! $this->_allowShapefileExport || ! $this->_canExportGeom()) {
            $res = $res->filter(fn ($k): bool => $k != 'shape')
                ->values();
        }

        $res = $res->sort()
            ->values();

        foreach ($this->_customExport as $k => $v) {
            $res = $res->add($k);
        }

        return $res->toArray();
    }

    private function _formatGeojson(): Response
    {
        if ($this->_flattenGeojsonResult) {
            $this->flattenResult();
        }

        $filename = $this->getExportFilename();
        $data = $this->get();
        return $this->geojson($data, $filename . '.geojson', $this->_geojsonFormatDataResourceClass ?? ListData::class);
    }

    private function _getAvailableFormats(): array
    {
        $res = ['list'];
        if ($this->_canExportGeom()) {
            $res[] = 'geojson';
        }

        foreach (array_keys($this->_customFormat) as $k) {
            $res[] = $k;
        }

        ksort($res);

        return $res;
    }

    private function _handleProcessing(Processing $processing): Response
    {
        $data = ProcessingData::from($processing);

        return response($this->formatResult($data->toArray()));
    }

    private function _returnExport(mixed $request): BinaryFileResponse|Response
    {
        $format = $request->input('export');

        if (! in_array($format, $this->_getAvailableExports())) {
            abort(HttpResponse::HTTP_BAD_REQUEST, $format . ' export is not allowed for this ressource.');
        }

        return $this->export($format);
    }

    private function _returnFormatResult(mixed $request, string $format = null): Response
    {
        $_format = $format ?? $request->input('format');

        return match ($_format) {
            'geojson' => $this->_handleFormatGeojson(),
            'list' => $this->_handleFormatList(),
            'position' => $this->_handleFormatPosition(),
            default => $this->_handleCustomFormat($_format),
        };
    }

    private static function _translateKey($item, string $translateKey): array
    {
        $res = [];
        foreach ($item as $k => $v) {
            $k = trans("{$translateKey}.{$k}");
            $res[$k] = $v;
        }

        return $res;
    }

    private function isExport(): bool
    {
        $request = request();
        return $request->has('export');
    }

    public function dataFormatClosure(Closure $func): static
    {
        $this->_customDataTransform = $func;
        return $this;
    }
}
