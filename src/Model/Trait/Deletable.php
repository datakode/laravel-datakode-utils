<?php

namespace Datakode\LaravelDatakodeUtils\Model\Trait;

use F9Web\LaravelDeletable\Traits\RestrictsDeletion;

trait Deletable
{
    use RestrictsDeletion;

    protected bool $deletable = true;

    public function isDeletable(): bool
    {
        return $this->deletable;
    }
}
