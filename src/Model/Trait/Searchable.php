<?php

namespace Datakode\LaravelDatakodeUtils\Model\Trait;

use Datakode\LaravelDatakodeUtils\Model\DatakodeApiModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\Grammars\PostgresGrammar;

/**
 * @method static Builder|DatakodeApiModel search($needle)
 */
trait Searchable
{
    public array $searchable = [];

    public function __toString(): string
    {
        $str = collect($this->searchable)
            ->map(function ($key) {
                return $this->getAttribute($key);
            })->implode(' - ');

        return $str !== '' ? $str : '__toString';
    }

    public function scopeSearch(Builder $query, $needle): Builder
    {
        $grammar = $query->getGrammar();
        $query->where(function ($subQuery) use ($grammar, $needle): void {
            foreach ($this->searchable as $key) {
                if ($grammar::class === PostgresGrammar::class) {
                    $subQuery = $subQuery->orWhere($this->getTable() . '.' . $key, 'ILIKE', '%' . $needle . '%');
                } else {
                    $subQuery = $subQuery->orWhere($this->getTable() . '.' . $key, 'like', '%' . $needle . '%');
                }
            }
        });

        return $query;
    }

    public function getDefaultOrderBy(): array
    {
        return count($this->searchable) > 0 ? $this->searchable : [$this->getKeyName()];
    }
}
