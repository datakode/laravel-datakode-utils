<?php

namespace Datakode\LaravelDatakodeUtils\Model\Trait;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes as BaseSoftDeletes;

trait SoftDeletes
{
    use BaseSoftDeletes;

    public function scopeHasTrashed(Builder $query, $value): Builder
    {
        return match ($value) {
            true => $query->onlyTrashed(), /** @phpstan-ignore-line "Call to an undefined method"*/
            'all' => $query->withTrashed(), /** @phpstan-ignore-line "Call to an undefined method"*/
            default => $query->withoutTrashed(), /** @phpstan-ignore-line "Call to an undefined method"*/
        };
    }
}
