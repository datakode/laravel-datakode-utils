<?php

namespace Datakode\LaravelDatakodeUtils\Model\Trait;

use Datakode\LaravelDatakodeUtils\Model\Trait\Userstamps\Listeners\Creating;
use Datakode\LaravelDatakodeUtils\Model\Trait\Userstamps\Listeners\Deleting;
use Datakode\LaravelDatakodeUtils\Model\Trait\Userstamps\Listeners\Updating;
use Wildside\Userstamps\Userstamps as BaseUserstamps;

trait Userstamps
{
    use BaseUserstamps;

    public static function registerListeners(): void
    {
        static::creating(Creating::class);
        static::updating(Updating::class);

        if (static::usingSoftDeletes()) {
            static::deleting(Deleting::class);
            //            static::restoring(Restoring::class);
        }
    }
}
