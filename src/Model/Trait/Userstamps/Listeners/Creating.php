<?php

namespace Datakode\LaravelDatakodeUtils\Model\Trait\Userstamps\Listeners;

class Creating
{
    /**
     * When the model is being created.
     */
    public function handle($model): void
    {
        if (! $model->isUserstamping() || $model->getCreatedByColumn() === null) {
            return;
        }

        if ($model->{$model->getCreatedByColumn()} === null) {
            $model->{$model->getCreatedByColumn()} = Auth('sanctum')
                ->id();
        }

        if ($model->{$model->getUpdatedByColumn()} !== null) {
            return;
        }

        if ($model->getUpdatedByColumn() === null) {
            return;
        }

        $model->{$model->getUpdatedByColumn()} = Auth('sanctum')
            ->id();
    }
}
