<?php

namespace Datakode\LaravelDatakodeUtils\Model\Trait\Userstamps\Listeners;

class Restoring
{
    /**
     * When the model is being restored.
     */
    public function handle($model): void
    {
        if (! $model->isUserstamping() || $model->getDeletedByColumn() === null) {
            return;
        }

        $model->{$model->getDeletedByColumn()} = null;
    }
}
