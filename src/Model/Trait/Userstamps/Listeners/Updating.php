<?php

namespace Datakode\LaravelDatakodeUtils\Model\Trait\Userstamps\Listeners;

class Updating
{
    /**
     * When the model is being updated.
     */
    public function handle($model): void
    {
        if (! $model->isUserstamping() || $model->getUpdatedByColumn() === null || Auth('sanctum')->id() === null) {
            return;
        }

        $model->{$model->getUpdatedByColumn()} = Auth('sanctum')
            ->id();
    }
}
