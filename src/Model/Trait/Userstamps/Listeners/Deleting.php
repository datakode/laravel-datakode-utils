<?php

namespace Datakode\LaravelDatakodeUtils\Model\Trait\Userstamps\Listeners;

class Deleting
{
    /**
     * When the model is being deleted.
     */
    public function handle($model): void
    {
        if (! $model->isUserstamping() || $model->getDeletedByColumn() === null) {
            return;
        }

        if ($model->{$model->getDeletedByColumn()} === null) {
            $model->{$model->getDeletedByColumn()} = Auth('sanctum')
                ->id();
        }

        $dispatcher = $model->getEventDispatcher();

        $model->unsetEventDispatcher();

        $model->save();

        $model->setEventDispatcher($dispatcher);
    }
}
