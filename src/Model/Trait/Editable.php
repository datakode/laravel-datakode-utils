<?php

namespace Datakode\LaravelDatakodeUtils\Model\Trait;

trait Editable
{
    protected bool $editable = true;

    public function isEditable(): bool
    {
        return $this->editable;
    }
}
