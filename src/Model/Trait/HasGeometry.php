<?php

namespace Datakode\LaravelDatakodeUtils\Model\Trait;

use Datakode\LaravelDatakodeUtils\Services\GeometryService;
use Exception;
use Illuminate\Contracts\Database\Query\Expression as ExpressionContract;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Support\Str;
use MatanYadaev\EloquentSpatial\Enums\Srid;
use MatanYadaev\EloquentSpatial\Objects\Geometry;
use MatanYadaev\EloquentSpatial\Objects\LineString;
use MatanYadaev\EloquentSpatial\Objects\MultiLineString;
use MatanYadaev\EloquentSpatial\Objects\MultiPoint;
use MatanYadaev\EloquentSpatial\Objects\MultiPolygon;
use MatanYadaev\EloquentSpatial\Objects\Point;
use MatanYadaev\EloquentSpatial\Objects\Polygon;
use MatanYadaev\EloquentSpatial\Traits\HasSpatial;
use Shapefile\Shapefile;

/**
 * @property Geometry|null $geometry
 *
 * @method getConnection()
 * @method getTable()
 */
trait HasGeometry
{
    use HasSpatial;

    public Srid $srid = Srid::WGS84;

    protected string $geometryField = 'geometry';

    public function getGeometryArray(): ?array
    {
        /** @var Geometry|null $geometry */
        $geometry = $this->{$this->geometryField};

        return $geometry?->toArray();
    }

    public function scopeWhereDistanceIsLessThan(Builder $query, $lat, $lng, $distance): Builder|QueryBuilder
    {
        $point = new Point($lat, $lng, $this->srid->value);
        $column = $this->getTable() . '.' . $this->geometryField;
        // TODO add hasGeometry interface to declare scope ...
        /** @noinspection PhpUndefinedMethodInspection */
        return $query->whereDistance($column, $point, '<', $distance) // @phpstan-ignore-line
            ->orderByDistance($column, $point)
            ->withDistance($column, $point, 'distance_is_less_than');
    }

    // -------------------------------
    // fix use geography for distances
    // -------------------------------

    public function scopeWithDistance(
        Builder $query,
        ExpressionContract|Geometry|string $column,
        ExpressionContract|Geometry|string $geometryOrColumn,
        string $alias = 'distance'
    ): void {
        if (! $query->getQuery()->columns) {
            $query->select('*');
        }

        $query->selectRaw(
            sprintf(
                'ST_DISTANCE(%s, %s) AS %s',
                $this->toExpressionStringGeography($column),
                $this->toExpressionStringGeography($geometryOrColumn),
                $alias,
            )
        );
    }

    protected function toExpressionStringGeography($geometryOrColumn): string
    {
        return Str::replaceLast('::geometry', '::geography', $this->toExpressionString($geometryOrColumn));
    }

    public function getBuffer(float $distance_meters, int $quad_segs = 10, Geometry $geometry = null): Geometry
    {
        if ($geometry === null) {
            $geometry = $this->geometry;
        }

        return (new GeometryService($geometry))->buffer($distance_meters, $quad_segs)
            ->geometry;
    }

    public function makeValid(Geometry $geometry = null): Geometry
    {
        if ($geometry === null) {
            $geometry = $this->geometry;
        }

        return (new GeometryService($geometry))->makeValid()
            ->geometry;
    }

    public function getBBox(Geometry $geometry = null): array
    {
        if ($geometry === null) {
            $geometry = $this->geometry;
        }

        return (new GeometryService($geometry))->getBBox();
    }

    public function getShapefileType(): int
    {
        switch ($this->geometry::class) {
            case LineString::class:
            case MultiLineString::class:
                return Shapefile::SHAPE_TYPE_POLYLINE;
            case Point::class:
            case MultiPoint::class:
                return Shapefile::SHAPE_TYPE_POINT;
            case Polygon::class:
            case MultiPolygon::class:
                return Shapefile::SHAPE_TYPE_POLYGON;
            default:
                throw new Exception('Geometry type ' . $this->geometry::class . ' not supported for ShapeType');
        }

    }

    public function getShapefileGeometry(): \Shapefile\Geometry\Geometry|null
    {
        /** @var Geometry|null $geometry */
        $geometry = $this->geometry;
        if (! $geometry) {
            return null;
        }

        $res = match ($geometry::class) {
            LineString::class => new \Shapefile\Geometry\Linestring(),
            MultiLineString::class => new \Shapefile\Geometry\MultiLinestring(),
            Point::class => new \Shapefile\Geometry\Point(),
            MultiPoint::class => new \Shapefile\Geometry\MultiPoint(),
            Polygon::class => new \Shapefile\Geometry\Polygon(),
            MultiPolygon::class => new \Shapefile\Geometry\MultiPolygon(),
            default => null
        };

        if ($res === null) {
            throw new Exception('Geometry type ' . $geometry::class . ' not supported for getShapefileGeometry');
        }

        return $res->initFromGeoJSON(json_encode($this->getGeometryArray()));
    }

    public function getWktProj(): string
    {
        $epsg4326 = 'GEOGCS["WGS 84",DATUM["WGS_1984",SPHEROID["WGS 84",6378137,298.257223563,AUTHORITY["EPSG","7030"]],AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.0174532925199433,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4326"]]'; // @see https://epsg.io/4326
        $epsg3857 = 'PROJCS["WGS 84 / Pseudo-Mercator",GEOGCS["WGS 84",DATUM["WGS_1984",SPHEROID["WGS 84",6378137,298.257223563,AUTHORITY["EPSG","7030"]],AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.0174532925199433,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4326"]],PROJECTION["Mercator_1SP"],PARAMETER["central_meridian",0],PARAMETER["scale_factor",1],PARAMETER["false_easting",0],PARAMETER["false_northing",0],UNIT["metre",1,AUTHORITY["EPSG","9001"]],AXIS["Easting",EAST],AXIS["Northing",NORTH],EXTENSION["PROJ4","+proj=merc +a=6378137 +b=6378137 +lat_ts=0 +lon_0=0 +x_0=0 +y_0=0 +k=1 +units=m +nadgrids=@null +wktext +no_defs"],AUTHORITY["EPSG","3857"]]'; // @see https://epsg.io/3857
        return match ($this->srid) {
            Srid::WGS84 => $epsg4326,
            Srid::WEB_MERCATOR => $epsg3857,
        };
    }
}
