<?php

namespace Datakode\LaravelDatakodeUtils\Model\Trait;

trait HasMetadata
{
    public function getMetas(): array
    {
        $res = [];
        if (method_exists($this, 'isDeletable')) {
            $res['deletable'] = $this->isDeletable();
        }

        if (method_exists($this, 'isEditable')) {
            $res['editable'] = $this->isEditable();
        }

        return $res;
    }
}
