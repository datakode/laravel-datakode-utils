<?php

namespace Datakode\LaravelDatakodeUtils\Model;

use Datakode\LaravelDatakodeUtils\DatakodeConfig;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

abstract class DatakodeUser extends DatakodeApiModel implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{
    use Authenticatable;
    use Authorizable;
    use CanResetPassword;
    use MustVerifyEmail;
    use Notifiable;

    /**
     * Must return guest user
     * ex:
     *
     * | public static function guest(): User
     * | {
     * | $user = new self();
     * | $role = Role::getRole(RoleEnum::guest);
     * | $user->attributes['role'] = $role->id;
     * |
     * | return $user;
     * | }
     */
    abstract public static function guest(): self;

    /**
     * Must return user permission ( collection of strings )
     * ex:
     *
     * | public function getPermissionsAttribute(): Collection
     * | {
     * |    return $this->role->permissions;
     * | }
     */
    abstract public function getPermissionsAttribute(): Collection;

    /**
     * Must all available permissions in app ( array of strings )
     *  ex:
     *
     *  | public static function getAvailablePermissionsStrings(): array
     *  | {
     *  |    return PermissionEnum::names();
     *  | }
     */
    abstract public static function getAvailablePermissionsStrings(): array;

    /**
     * Critères utilisés pour mettre en cache les résultats de DatakodeQueryBuilder (en plus de l'url)
     */
    public function getDatakodeQueryBuilderCacheKey(): array
    {
        $user_id = $this->getKey();

        return compact('user_id');
    }

    /**
     * return only available fields for a dataResource
     *
     * @param string $name date resource name
     * @param Collection $fields array of fields
     */
    public static function getAvailableFieldsFor(string $name, Collection $fields): Collection
    {
        return $fields->filter(function ($field) use ($name): bool {
            return ! self::isDisabledField($name, $field);
        });
    }

    /**
     * return only allowed fields for a dataResource
     *
     * @param string $name date resource name
     * @param Collection $fields array of fields
     */
    public function getOnlyReadableFieldsFor(string $name, Collection $fields): Collection
    {
        return $fields->filter(function ($field) use ($name): bool {
            return $this->hasReadAccessToField($name, $field);
        });
    }

    /**
     * return only writable fields for a dataResource
     *
     * @param string $name date resource name
     * @param Collection $fields array of fields
     */
    public function getOnlyWritableFieldsFor(string $name, Collection $fields): Collection
    {
        return $fields->filter(function ($field) use ($name): bool {
            return $this->hasWriteAccessToField($name, $field);
        });
    }

    public function hasPermissionString(string $permission): bool
    {
        $permissions = $this->getPermissionsStrings();

        $res = $permissions->contains($permission);

        if (config('laravel-datakode-utils.permissions.logPermissionChecks')) {
            if ($res) {
                Log::debug($permission . ' [OK]');
            } else {
                Log::debug($permission . ' [DENIED]');
            }
        }

        return $res;
    }

    /**
     * @param string $model date resource name (ex: User::class)
     * @param string $field field name
     */
    public function hasReadAccessToField(string $model, string $field): bool
    {
        // check preDot access 'bar.foo' must check access to 'bar' before check access to 'bar.foo'
        $dotSplit = explode('.', $field);
        if (count($dotSplit) > 1) {
            $preDot = Str::of($field)->beforeLast('.');
            if (! $this->hasReadAccessToField($model, $preDot)) {
                return false;
            }
        }

        return $this->hasAccessToField(
            $model,
            $field,
            DatakodeConfig::configOrFail('laravel-datakode-utils.permissions.readFieldPermissionTemplate')
        );
    }

    /**
     * @param string $model date resource name (ex: User::class)
     * @param string $field field name
     */
    public function hasWriteAccessToField(string $model, string $field): bool
    {
        // check preDot access 'bar.foo' must check access to 'bar' before check access to 'bar.foo'
        $dotSplit = explode('.', $field);
        if (count($dotSplit) > 1) {
            $preDot = Str::of($field)->beforeLast('.');
            if (! $this->hasWriteAccessToField($model, $preDot)) {
                return false;
            }
        }

        return $this->hasReadAccessToField($model, $field)
            && $this->hasAccessToField(
                $model,
                $field,
                DatakodeConfig::configOrFail('laravel-datakode-utils.permissions.writeFieldPermissionTemplate')
            );
    }

    public function hasPermissionIfExists(string $permissionKey): bool
    {
        $permission = collect(static::getAvailablePermissionsStrings())->search($permissionKey) !== false;

        if (! $permission) {
            if (config('laravel-datakode-utils.permissions.logPermissionChecks')) {
                Log::debug($permissionKey . ' [permission not found]');
            }

            // si la permission n'existe pas dans l'app c'est qu'on a le droit
            return true;
        }

        return $this->hasPermissionString($permissionKey);
    }

    /**
     * @param string $model date resource name (ex: User::class)
     * @param string $field field name
     */
    public static function isDisabledField(string $model, string $field): bool
    {
        if (Str::of($field)->endsWith('_id') && self::isDisabledField($model, Str::beforeLast($field, '_id'))) {
            return true;
        }

        $key = Str::snake(class_basename($model)) . '.' . Str::snake($field);
        $disabledFields = collect(config('laravel-datakode-utils.disabled_fields', []))
            ->map(function ($t) {
                return Str::of($t)->trim()->snake();
            });

        return (bool) $disabledFields->first(function ($disabledField) use ($key) {
            // disabled all field ending like the disable field param
            return Str::of($key)->endsWith($disabledField);
        });
    }

    /**
     * @return Collection<string>
     */
    protected function getPermissionsStrings(): Collection
    {
        return $this->getPermissionsAttribute()
            ->map(function ($v) {
                return is_string($v) ? $v : $v->name;
            });
    }

    /**
     * @param string $model date resource name (ex: User::class)
     * @param string $field field name
     */
    protected function hasAccessToField(string $model, string $field, $permissionTemplate): bool
    {
        if (self::isDisabledField($model, $field)) {
            return false;
        }

        $model = Str::camel(class_basename($model));
        $field = Str::camel($field);
        $permission = Str::of($permissionTemplate)
            ->replace('{model}', $model)
            ->replace('{field}', $field);

        return $this->hasPermissionIfExists($permission);
    }
}
