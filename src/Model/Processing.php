<?php

namespace Datakode\LaravelDatakodeUtils\Model;

use Datakode\LaravelDatakodeUtils\DatakodeAuth;
use Datakode\LaravelDatakodeUtils\DatakodeConfig;
use Datakode\LaravelDatakodeUtils\DatakodeQueryBuilder;
use Datakode\LaravelDatakodeUtils\DatakodeRoute;
use Datakode\LaravelDatakodeUtils\Events\ProcessingEndEvent;
use Datakode\LaravelDatakodeUtils\Model\Trait\Userstamps;
use Datakode\LaravelDatakodeUtils\Services\ExportService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Override;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @property int $id
 * @property int $cpt
 * @property int $max
 * @property string $key
 * @property string $hash
 * @property string|null $name
 * @property string $process_class
 * @property array $parameters
 * @property string|null $status
 * @property string|null $info
 * @property string|null $storage
 * @property string|null $filename
 * @property string|null $mime_type
 * @property bool $mail_me
 * @property bool $mail_sent
 * @property bool $obsolete
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $started_at
 * @property Carbon|null $terminated_at
 * @property Carbon|null $downloaded_at
 * @property-read DatakodeUser|null $creator
 * @property-read DatakodeUser|null $destroyer
 * @property-read DatakodeUser|null $editor
 * @property-read bool $deletable
 * @property-read string|null $download_url
 * @property-read int $duration
 * @property-read int|null $estimated_duration
 * @property-read string $k
 * @property-read float $percent
 * @property-read string $t
 * @property-read string $url
 *
 * @method static Builder|Processing newModelQuery()
 * @method static Builder|Processing newQuery()
 * @method static Builder|Processing query()
 * @method static Builder|DatakodeApiModel search($needle)
 * @method static Builder|Processing whereCpt($value)
 * @method static Builder|Processing whereCreatedAt($value)
 * @method static Builder|Processing whereCreatedBy($value)
 * @method static Builder|Processing whereDownloadedAt($value)
 * @method static Builder|Processing whereFilename($value)
 * @method static Builder|DatakodeApiModel whereHasBooleanValue(string $attribute, string|bool $value)
 * @method static Builder|DatakodeApiModel whereHasRelation(string $relation, string|bool $value)
 * @method static Builder|Processing whereHash($value)
 * @method static Builder|Processing whereId($value)
 * @method static Builder|Processing whereInfo($value)
 * @method static Builder|Processing whereKey($value)
 * @method static Builder|Processing whereMailMe($value)
 * @method static Builder|Processing whereMailSent($value)
 * @method static Builder|Processing whereMax($value)
 * @method static Builder|Processing whereMimeType($value)
 * @method static Builder|Processing whereName($value)
 * @method static Builder|Processing whereObsolete($value)
 * @method static Builder|Processing whereParameters($value)
 * @method static Builder|Processing whereProcessClass($value)
 * @method static Builder|Processing whereStartedAt($value)
 * @method static Builder|Processing whereStatus($value)
 * @method static Builder|Processing whereStorage($value)
 * @method static Builder|Processing whereTerminatedAt($value)
 * @method static Builder|Processing whereUpdatedAt($value)
 * @method static Builder|Processing whereUpdatedBy($value)
 */
class Processing extends DatakodeApiModel
{
    use Userstamps;

    public array $searchable = ['name'];

    protected $attributes = [
        'cpt' => 0,
        'max' => 1,
        'mail_me' => false,
        'status' => 'new',
    ];

    protected $appends = ['url', 'percent', 'duration', 'estimated_duration', 'download_url'];

    protected $fillable = ['mail_me'];

    protected bool $deletable = false;

    #[Override]
    public function __toString(): string
    {
        return (string) $this->name;
    }

    public static function init(
        DatakodeQueryBuilder $queryBuilder,
        string $processingClass,
        array $parameters
    ): self {
        $res = new (DatakodeConfig::configOrFail('laravel-datakode-utils.processing.model'))();
        $res->key = $queryBuilder->getCacheKey();
        $res->hash = md5($queryBuilder->getCacheKey() . Carbon::now()->unix());
        $format = Arr::get($parameters, 'format');
        $res->name = class_basename($processingClass) . ' ' . class_basename($queryBuilder->getModel()) . ' ' . $format;
        $res->process_class = $processingClass;
        $res->parameters = $parameters;
        $res->save();

        $debugExport = config('laravel-datakode-utils.debug_export');

        if ($debugExport) {
            $job = new $processingClass($res, $queryBuilder, DatakodeAuth::currentUser(), $parameters);
            $job->handle(new ExportService());
        } else {
            $processingClass::dispatch($res, $queryBuilder, DatakodeAuth::currentUser(), $parameters);
        }

        return $res;
    }

    public function getPercentAttribute(): float
    {
        return $this->cpt / $this->max * 100;
    }

    public function getDurationAttribute(): int
    {
        if ($this->terminated_at) {
            return (int) round($this->started_at->diffInSeconds($this->terminated_at));
        }

        return (int) round(-1 * Carbon::now()->diffInSeconds($this->started_at));
    }

    public function getEstimatedDurationAttribute(): ?float
    {
        return $this->cpt > 0 ? (int) round($this->duration * ($this->max / $this->cpt)) : null;
    }

    public function setCpt(int $int, ?string $info = null): void
    {
        $this->cpt = $int;
        $this->max = max($int, $this->max);
        if ($info) {
            $this->info = $info;
        }

        if ($this->started_at === null) {
            $this->started_at = Carbon::now();
            $this->status = 'running';
        }

        $this->save();
    }

    public function setMaxCpt(int $int): void
    {
        $this->max = $int;
        $this->save();
    }

    public function setInfo(string $info): void
    {
        $this->info = $info;
        $this->save();
    }

    public function end(string $storage): void
    {
        $this->terminated_at = Carbon::now();
        $this->storage = $storage;
        $this->cpt = $this->max;
        $this->status = 'done';
        $this->save();
        event(new ProcessingEndEvent($this));
    }

    public function store(mixed $res, string $mime_type, string $filename): string
    {
        $this->mime_type = $mime_type;
        $this->filename = $filename;
        $key = $this->_getStoragePath();

        if ($res instanceof BinaryFileResponse) {
            $res = $res->getFile();
        }

        if ($res instanceof Response || $res instanceof File) {
            $res = $res->getContent();
        }

        Storage::put($key, $res);

        return $key;
    }

    public function downloadFile(): Response
    {
        $content = Storage::get($this->_getStoragePath());
        $contentType = $this->mime_type;
        $filename = $this->filename;
        $response = response($content, 200);
        $response->header('Content-Type', $contentType);
        $response->header('Content-Disposition', 'attachment; filename="' . $filename . '"');

        return $response;
    }

    public function getStorageFolder(): string
    {
        $path = 'exports/';
        Storage::makeDirectory($path);

        return $path;
    }

    public function getUrlAttribute(): string
    {
        return DatakodeRoute::trimApiBaseUrl(route('processings.show', $this->hash));
    }

    public function getDownloadUrlAttribute(): ?string
    {
        return $this->status === 'done' ?
            DatakodeRoute::trimApiBaseUrl(route('processings.download', $this->hash))
            : null;
    }

    #[Override]
    protected function casts(): array
    {
        return [
            'parameters' => 'json',
            'started_at' => 'datetime',
            'terminated_at' => 'datetime',
            'downloaded_at' => 'datetime',
            'mail_me' => 'boolean',
            'mail_sent' => 'boolean',
            'obsolete' => 'boolean',
        ];
    }

    private function _getStoragePath(): string
    {
        return $this->getStorageFolder() . 'export_' . $this->id;
    }
}
