<?php

namespace Datakode\LaravelDatakodeUtils\Model;

use Datakode\LaravelDatakodeUtils\Model\Trait\Deletable;
use Datakode\LaravelDatakodeUtils\Model\Trait\Editable;
use Datakode\LaravelDatakodeUtils\Model\Trait\HasMetadata;
use Datakode\LaravelDatakodeUtils\Model\Trait\Searchable;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Override;

/**
 * DatakodeUtils\Model\DatakodeApiModel
 *
 * @property-read bool $deletable
 * @property-read string $k
 * @property-read string $t
 *
 * @method static Builder|DatakodeApiModel newModelQuery()
 * @method static Builder|DatakodeApiModel newQuery()
 * @method static Builder|DatakodeApiModel query()
 * @method static Builder|DatakodeApiModel search($needle)
 * @method static Builder|DatakodeApiModel whereHasBooleanValue(string $attribute, string|bool $value)
 * @method static Builder|DatakodeApiModel whereHasRelation(string $relation, string|bool $value)
 */
/** @phpstan-consistent-constructor */
class DatakodeApiModel extends Model
{
    use Deletable;
    use Editable;
    use HasMetadata;
    use Searchable;

    protected $appends = ['k', 't'];

    protected ?string $routeKey = null;

    protected static bool $clearCacheOnChange = false;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    #[Override]
    public function getRouteKeyName()
    {
        return $this->routeKey ?? parent::getRouteKeyName();
    }

    public function getKAttribute(): string
    {
        return (string) $this->getRouteKey();
    }

    public function getTAttribute(): string
    {
        return (string) $this;
    }

    public function getDeletableAttribute(): bool
    {
        return $this->isDeletable();
    }

    // ---- scopes

    public function scopeWhereHasRelation(
        Builder|QueryBuilder $query,
        string $relation,
        string|bool $value
    ): Builder|QueryBuilder {
        return match ($value) {
            true => $query->whereHas($relation),
            'all' => $query,
            default => $query->doesntHave($relation),
        };
    }

    public function scopeWhereHasBooleanValue(
        Builder|QueryBuilder $query,
        string $attribute,
        string|bool $value
    ): Builder|QueryBuilder {
        return match ($value) {
            true => $query->where($attribute, true),
            'all' => $query,
            default => $query->where($attribute, false),
        };
    }

    /**
     * get table name
     */
    public static function getTableName(): string
    {
        $static = new static();
        if (! method_exists($static, 'getTable')) {
            throw new Exception('Model as not table');
        }

        return $static->getTable();
    }

    /**
     * get full field path
     * ex: User::getTableFieldName('id') => users.id
     */
    public static function getTableFieldName(string $field): string
    {
        return static::getTableName() . '.' . $field;
    }

    #[Override]
    protected static function boot(): void
    {
        parent::boot();

        if (static::$clearCacheOnChange) {
            static::creating(static function ($model): void {
                self::clearCache($model, 'creating');
            });
            static::updating(static function ($model): void {
                self::clearCache($model, 'updating');
            });
            static::deleting(static function ($model): void {
                self::clearCache($model, 'deleting');
            });
        }
    }

    protected static function clearCache(Model $model = null, string $event = '?'): void
    {
        Cache::clear();
        Log::debug(
            'DatakodeApiModel cache cleared by "' . static::class . '" [' . $model?->getKeyName() . ':' . $model?->getKey() . '] ' . $event
        );
    }

    #[Override]
    public static function withoutEvents(callable $callback)
    {
        if (static::$clearCacheOnChange) {
            self::clearCache(null, 'withoutEvents');
        }

        return parent::withoutEvents($callback);
    }
}
