<?php

namespace Datakode\LaravelDatakodeUtils\Listeners;

use Datakode\LaravelDatakodeUtils\Events\ProcessingEndEvent;
use Datakode\LaravelDatakodeUtils\Model\Processing;
use Datakode\LaravelDatakodeUtils\Notifications\ProcessingEndNotification;

class SendProcessEndNotificationListener
{
    public function handle(ProcessingEndEvent $event): void
    {
        /** @var Processing $processing */
        $processing = Processing::whereId(
            $event->processing->id
        )->firstOrFail(); // force reload data if changed (please do not use ->refresh())

        if ($processing->mail_me) {
            $user = $processing->creator;
            $user->notify(new ProcessingEndNotification($processing));
        }
    }
}
