<?php

namespace Datakode\LaravelDatakodeUtils\Controller;

use Datakode\LaravelDatakodeUtils\DatakodeAuth;
use Datakode\LaravelDatakodeUtils\Events\ProcessingEndEvent;
use Datakode\LaravelDatakodeUtils\LaravelData\ProcessingData;
use Datakode\LaravelDatakodeUtils\Model\Processing;
use Datakode\LaravelDatakodeUtils\Requests\ProcessingRequest;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Override;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class ProcessingsController extends DatakodeBaseController
{
    /**
     * Index
     */
    public function index(): Response|BinaryFileResponse
    {
        $user = DatakodeAuth::currentUser();
        $query = Processing::whereCreatedBy($user?->getKey());

        return $this->listEntities(Processing::class, $query)
            ->setDataResource(ProcessingData::class)
            ->useCache(false)
            ->getCollection();
    }

    /**
     * Show
     */
    public function show(string $hash): array
    {
        $user = DatakodeAuth::currentUser();
        /** @var Processing $processing */
        $processing = Processing::whereHash($hash)
            ->where('created_by', '=', $user?->getKey())
            ->firstOrFail();

        return $this->showEntity($processing, ProcessingData::class, new ProcessingRequest());
    }

    /**
     * Show
     */
    public function update(string $hash, ProcessingRequest $request): array
    {
        $user = DatakodeAuth::currentUser();
        /** @var Processing $processing */
        $processing = Processing::whereHash($hash)
            ->where('created_by', '=', $user?->getKey())
            ->firstOrFail();

        return $this->updateEntity($processing, $request, ProcessingData::class, function (
            Processing $entity,
            array $data
        ): Processing {
            $entity->update($data);

            if ($entity->status === 'done' && $entity->mail_me && ! $entity->mail_sent) {
                // trigger a new ProcessingEndEvent event if process is updated after it ends
                event(new ProcessingEndEvent($entity));
            }

            return $entity;
        });
    }

    /**
     * Download
     */
    public function download(string $hash): Response
    {
        $user = DatakodeAuth::currentUser();
        /** @var Processing $processing */
        $processing = Processing::whereHash($hash)
            ->where('created_by', '=', $user?->getKey())
            ->where('terminated_at', '<>', null)
            ->firstOrFail();

        $res = $processing->downloadFile();
        $processing->downloaded_at = Carbon::now();
        $processing->save();

        return $res;
    }

    #[Override]
    public function gatePermission(string $permission): void
    {
        // no gate for processings
    }
}
