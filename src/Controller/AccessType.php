<?php

namespace Datakode\LaravelDatakodeUtils\Controller;

enum AccessType
{
    case index;
    case list; // allow index endpoint for only format=list (used for autocomplete)
    case show;
    case store;
    case update;
    case destroy;
    case restore;
    case export;
}
