<?php

namespace Datakode\LaravelDatakodeUtils\Controller;

use Carbon\Carbon;
use Composer\InstalledVersions;
use Datakode\LaravelDatakodeUtils\DatakodeHealthCheck;
use Illuminate\Foundation\Events\DiagnosingHealth;
use Illuminate\Support\Facades\Event;
use Override;

class ApiInfoController extends DatakodeBaseController
{
    public function home(): array
    {
        $packageData = $this->_getpackageData();
        $path = app_path('../composer.json');
        $build_date = new Carbon(date('F d Y H:i:s.', filemtime($path)));
        $build_date = $build_date->toW3cString();

        $env = config('app.env');
        $releases_url = config('app.releases_url');

        return [...$packageData, ...compact('build_date', 'env', 'releases_url')];
    }

    public function healthCheck(): array
    {
        if (! defined('LARAVEL_START')) {
            define('LARAVEL_START', microtime(true));
        }

        Event::dispatch(new DiagnosingHealth());
        $ok = true;
        $tests = DatakodeHealthCheck::getTestsResults();
        $render_time_ms = round((microtime(true) - LARAVEL_START) * 1000);
        return compact('ok', 'tests', 'render_time_ms');
    }

    #[Override]
    public function gatePermission(string $permission): void
    {
        // no gate for processings
    }

    private function _getpackageData(): array
    {
        return Collect(InstalledVersions::getRootPackage())
            ->only(['name', 'pretty_version', 'version'])
            ->toArray();
    }
}
