<?php

/** @noinspection PhpUnused */

namespace Datakode\LaravelDatakodeUtils\Controller;

use Carbon\Carbon;
use Closure;
use Datakode\LaravelDatakodeUtils\DatakodeApiResponse;
use Datakode\LaravelDatakodeUtils\DatakodeAuth;
use Datakode\LaravelDatakodeUtils\DatakodeConfig;
use Datakode\LaravelDatakodeUtils\DatakodeFormRequest;
use Datakode\LaravelDatakodeUtils\DatakodeModelQueryBuilder;
use Datakode\LaravelDatakodeUtils\DatakodeQueryBuilder;
use Datakode\LaravelDatakodeUtils\LaravelData\DatakodeData;
use Datakode\LaravelDatakodeUtils\LaravelData\DatakodeDataBag;
use Datakode\LaravelDatakodeUtils\Model\DatakodeApiModel;
use Datakode\LaravelDatakodeUtils\Model\DatakodeUser;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Foundation\Application;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
use RuntimeException;

abstract class DatakodeBaseController extends Controller
{
    use AuthorizesRequests;
    use ValidatesRequests;

    /**
     * @var array<string, Closure>
     */
    private array $_customFormat = [];

    public function getPermissionName(string $modelClassName, string $permissionTemplate): string
    {
        $model = Str::camel(class_basename($modelClassName));

        return Str::of($permissionTemplate)->replace('{model}', $model)->toString();
    }

    abstract public function gatePermission(string $permission): void;

    public function gateEntityAccess(string $modelClassName, AccessType $accessType): void
    {
        $this->gateEntityAccessWithTemplate($modelClassName, $this->getTemplateForAccessType($accessType));
    }

    public function success(string $success, array $data = [], array $meta = []): array
    {
        return (new DatakodeApiResponse($data))->addMetas($meta)
            ->success($success)
            ->toArray();
    }

    public function error(string|array $error, array $data = []): array
    {
        return compact('data', 'error');
    }

    public function errorResponse(array $errors, string $message = null, array $data = [], int $status = 400): Response
    {
        return (new DatakodeApiResponse($data))->errors($errors, $message, $status)
            ->response();
    }

    public function addFormat(string $name, Closure $method): static
    {
        $this->_customFormat[$name] = $method;

        return $this;
    }

    protected function listEntities(
        string $modelClass,
        Builder|Relation|string|null $subject = null,
        ?Request $request = null
    ): DatakodeQueryBuilder {

        if (request()->get('format') !== 'list' || ! $this->getEntityAccess(
            modelClassName: $modelClass,
            accessType: AccessType::list,
            returnTrueIfPermissionNotExist: false
        )) {
            $this->gateEntityAccess($modelClass, AccessType::index);
        }

        if (! is_subclass_of($modelClass, Model::class)) {
            return throw new RuntimeException($modelClass . ' is not a subclass of ' . Model::class);
        }

        /** @var Builder $subjectBuilder */
        $subjectBuilder = $subject ?? $modelClass::query();

        $allowExport = $this->getEntityAccess($modelClass, AccessType::export);

        return DatakodeQueryBuilder::forModel($modelClass, $subjectBuilder, $request, $allowExport);
    }

    protected function listEntitiesUsingQueryBuilder(string $queryBuilderClass): DatakodeQueryBuilder
    {
        if (! is_subclass_of($queryBuilderClass, DatakodeModelQueryBuilder::class)) {
            return throw new RuntimeException(
                $queryBuilderClass . ' is not a subclass of ' . DatakodeModelQueryBuilder::class
            );
        }

        $modelClass = $queryBuilderClass::getModelClass();
        $this->gateEntityAccess($modelClass, AccessType::index);
        if (! is_subclass_of($modelClass, Model::class)) {
            return throw new RuntimeException($modelClass . ' is not a subclass of ' . Model::class);
        }

        $allowExport = $this->getEntityAccess($modelClass, AccessType::export);

        return $queryBuilderClass::getQueryBuilder($allowExport);
    }

    protected function showEntity(
        DatakodeApiModel $entity,
        null|string|array|DatakodeDataBag $dataClass = null,
        ?DatakodeFormRequest $formRequest = null,
        ?string $htmlTemplate = null
    ): array|Response|View {
        $this->gateEntityAccess($entity::class, AccessType::show);
        $data = self::getData($dataClass, $entity);

        $request = request();
        if ($request->has('format')) {
            // handle formats
            $format = $request->input('format');

            return match ($format) {
                'html' => $this->handleFormatHtml($data->toArray(), $htmlTemplate),
                'pdf' => $this->handleFormatPdf($data->toArray(), $htmlTemplate),
                default => $this->_handleCustomFormat($format, $data->toArray()),
            };
        }

        $putRules = $formRequest ? [
            'rules' => $formRequest->getRulesArrayMeta(),
        ] : [];

        $meta = [...$putRules, ...$entity->getMetas()];

        if (defined('LARAVEL_START')) {
            $meta['render_time_ms'] = round((microtime(true) - constant('LARAVEL_START')) * 1000);
        }

        $meta['time'] = Carbon::now()->toJSON();

        return $this->success($this->_getEntityClass($entity) . ' reached.', $data->toArray(), $meta);
    }

    protected function updateEntity(
        DatakodeApiModel $entity,
        DatakodeFormRequest $formRequest,
        null|string|array|DatakodeDataBag $dataClass = null,
        ?callable $updateMethod = null
    ): array {
        $this->gateEntityAccess($entity::class, AccessType::update);
        $data = $this->filterAndValidate($formRequest);

        if ($updateMethod) {
            $entity = $updateMethod($entity, $data);
        } else {
            $entity->update($data);
        }

        $data = self::getData($dataClass, $entity);

        return $this->success(
            $this->_getEntityClass($entity) . ' updated.',
            $data->toArray(),
            [
                'rules' => $formRequest->getRulesArrayMeta(),
                ...$entity->getMetas(),
            ]
        );
    }

    protected function showCreate(
        string $modelClass,
        null|string|array $dataClass = null,
        ?DatakodeFormRequest $formRequest = null,
        ?callable $createMethod = null,
        array $defaultData = []
    ): array {
        $this->gateEntityAccess($modelClass, AccessType::store);
        if (is_array($dataClass)) {
            $dataClass = $dataClass[0];
        }

        if ($dataClass && ! is_subclass_of($dataClass, DatakodeData::class)) {
            return throw new RuntimeException($dataClass . ' is not a subclass of ' . DatakodeData::class);
        }

        $data = $dataClass ? $dataClass::empty($defaultData) : new $modelClass();

        if ($createMethod) {
            $data = $createMethod($data);
        }

        $putRules = $formRequest ? [
            'rules' => $formRequest->getRulesArrayMeta(),
        ] : [];

        return $this->success(class_basename($modelClass) . ' empty object.', $data, $putRules);
    }

    protected function storeEntity(
        string $modelClassStr,
        DatakodeFormRequest $formRequest,
        null|string|array|DatakodeDataBag $dataClass = null,
        ?callable $storeMethod = null
    ): array {
        $this->gateEntityAccess($modelClassStr, AccessType::store);
        if (! is_subclass_of($modelClassStr, DatakodeApiModel::class)) {
            return throw new RuntimeException($modelClassStr . ' is not a subclass of ' . Model::class);
        }

        /** @var DatakodeApiModel $modelClass */
        $modelClass = $modelClassStr;
        $data = $this->filterAndValidate($formRequest);
        if ($storeMethod) {
            $entity = $storeMethod($data);
        } else {
            $entity = new $modelClass();
            $entity->fill($data);
            $entity->save();
        }

        $data = self::getData($dataClass, $entity);

        return $this->success(
            $this->_getEntityClass($entity) . ' created.',
            $data->toArray(),
            [
                'rules' => $formRequest->getRulesArrayMeta(),
                ...$entity->getMetas(),
            ]
        );
    }

    protected function destroyEntity(DatakodeApiModel $entity, ?callable $deleteMethod = null): array
    {
        $this->gateEntityAccess($entity::class, AccessType::destroy);
        if ($deleteMethod) {
            $deleteMethod($entity);
        } else {
            $entity->delete();
        }

        return $this->success($this->_getEntityClass($entity) . ' deleted.');
    }

    protected function restoreEntity(Model $entity, ?callable $restoreMethod = null): array
    {
        $this->gateEntityAccess($entity::class, AccessType::restore);
        if ($restoreMethod) {
            $restoreMethod($entity);
        } else {
            if (! method_exists($entity, 'restore')) {
                throw new RuntimeException('Entity must implement restore method.');
            }

            $entity->restore();
        }

        return $this->success($this->_getEntityClass($entity) . ' restored.');
    }

    protected function filterAndValidate(DatakodeFormRequest $formRequest): array
    {
        //        return $formRequest->validate($formRequest->getRulesArray());
        return $formRequest->validate($formRequest->getFilteredRules()->toArray());
    }

    protected function getUser(): DatakodeUser
    {
        /** @var DatakodeUser|null $user */
        $user = DatakodeAuth::currentUser();
        if (! $user) {
            throw new RuntimeException('User not found.');
        }

        return $user;
    }

    protected function handleFormatHtml(
        array $data,
        ?string $template = null
    ): \Illuminate\Contracts\Foundation\Application|Factory|View|Application {
        if ($template === null) {
            $template = 'default';
        }

        return view($template, compact('data'));
    }

    protected function handleFormatPdf(array $data, ?string $template = null): Response
    {
        $pdf = App::make('dompdf.wrapper');

        return $pdf->loadView($template, compact('data'))
            ->download('test.pdf'); // TODO get filename from route
    }

    protected static function getData(
        array|string|null|DatakodeDataBag $dataClass,
        DatakodeApiModel $entity
    ): DatakodeApiModel|DatakodeData {
        if (! $dataClass) {
            return $entity;
        }

        return (new DatakodeDataBag($dataClass))->getData($entity);
    }

    /**
     * Utils
     */
    private function gateEntityAccessWithTemplate(string $modelClassName, string $permissionTemplate): void
    {
        $permission = $this->getPermissionName($modelClassName, $permissionTemplate);

        $this->gatePermission($permission);
    }

    private function getTemplateForAccessType(AccessType $accessType): string
    {
        $template = "laravel-datakode-utils.permissions.{$accessType->name}EntityTemplate";
        return DatakodeConfig::configOrFail($template);
    }

    private function _getEntityClass(Model $entity): string
    {
        return class_basename($entity::class);
    }

    private function _handleCustomFormat(string $format, array $data): Response
    {
        $method = Arr::get($this->_customFormat, $format);
        if ($method === null) {
            throw new RuntimeException('Unhandled format: ' . $format);
        }

        return $method($this);
    }

    private function getEntityAccess(
        string $modelClassName,
        AccessType $accessType,
        bool $returnTrueIfPermissionNotExist = true
    ): bool {
        $template = $this->getTemplateForAccessType($accessType);
        $permission = $this->getPermissionName($modelClassName, $template);

        $user = $this->getUser();
        return $returnTrueIfPermissionNotExist ? $user->hasPermissionIfExists($permission) : $user->hasPermissionString(
            $permission
        );
    }
}
