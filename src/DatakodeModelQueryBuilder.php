<?php

/** @noinspection PhpUnused */

namespace Datakode\LaravelDatakodeUtils;

use Datakode\LaravelDatakodeUtils\Model\DatakodeUser;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Response;

abstract class DatakodeModelQueryBuilder extends DatakodeQueryBuilder
{
    abstract public static function getModelClass(): string;

    public static function getQueryBuilder(bool $allowExport): DatakodeQueryBuilder
    {
        $res = static::forModel(
            modelClass: static::getModelClass(), // ok! false error warning : "Cannot call abstract method"
            subject: static::getBuilderQuery(), // ok! false error warning : "Cannot call abstract method"
            allowExport: $allowExport
        );

        $res->allowedSorts($res->getAllowedSorts())
            ->allowedFilters($res->getAllowedFilters());

        $defaultSort = $res->getDefaultSort();
        if ($defaultSort) {
            $res->defaultSort($defaultSort);
        }

        return $res;
    }

    public function errorResponse(array $error, string $message = null, array $data = [], int $status = 400): Response
    {
        return (new DatakodeApiResponse($data))->errors($error, $message, $status)
            ->response();
    }

    abstract protected static function getBuilderQuery(): Builder;

    abstract protected function getAllowedSorts(): array;

    abstract protected function getAllowedFilters(): array;

    abstract protected function getDefaultSort(): ?string;

    protected static function userHasPermission(string $permission): bool
    {
        /** @var DatakodeUser|null $user */
        $user = DatakodeAuth::currentUser();

        return $user->hasPermissionString($permission);
    }
}
