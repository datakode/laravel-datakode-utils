<?php

namespace Datakode\LaravelDatakodeUtils\LaravelData;

use Datakode\LaravelDatakodeUtils\Model\DatakodeApiModel;
use Override;

class ListData extends DatakodeData
{
    protected DatakodeApiModel $entity;

    public function __construct(
        public mixed $k,
        public string $v,
        public string $id,
    ) {
    }

    public static function fromModel(DatakodeApiModel $entity): self
    {
        $res = new self($entity->getKAttribute(), (string) $entity, $entity->getKey());

        $res->entity = $entity;

        return $res;
    }

    #[Override]
    public function toArray(): array
    {
        return [
            ...parent::toArray(),
            ...[
                $this->entity->getRouteKeyName() => $this->entity->getRouteKey(),
            ],
        ];
    }
}
