<?php

namespace Datakode\LaravelDatakodeUtils\LaravelData;

use Datakode\LaravelDatakodeUtils\DatakodeAuth;
use Datakode\LaravelDatakodeUtils\Model\DatakodeApiModel;
use Illuminate\Pagination\AbstractCursorPaginator;
use Illuminate\Pagination\AbstractPaginator;
use Illuminate\Pagination\CursorPaginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Enumerable;
use Illuminate\Support\LazyCollection;
use RuntimeException;
use Spatie\LaravelData\CursorPaginatedDataCollection;
use Spatie\LaravelData\DataCollection;
use Spatie\LaravelData\PaginatedDataCollection;
use UnitEnum;

class DatakodeDataBag
{
    /**
     * @var UnitEnum[]
     */
    public array $scopes = [];

    public ?string $dataClassName;

    public function __construct(
        array|string|null|self $mixed,
    ) {
        if ($mixed instanceof self) {
            $this->_setDataClass($mixed->dataClassName);
            $this->setScopes(...$mixed->scopes);
            return;
        }

        if (is_array($mixed)) {
            $this->_setDataClass($mixed[0]);
            $dataClassCollection = collect($mixed);
            $dataClassCollection->shift();

            $this->setScopes(...$dataClassCollection->values()->toArray());
            return;
        }

        $this->_setDataClass($mixed);
    }

    private function _setDataClass(string $dataClass): void
    {
        if ($dataClass && ! is_subclass_of($dataClass, DatakodeData::class)) {
            throw new RuntimeException($dataClass . ' is not a subclass of ' . DatakodeData::class);
        }

        $this->dataClassName = $dataClass;
    }

    public function setScopes(UnitEnum ...$scopes): self
    {
        /** @var DatakodeData $dataClass */
        $dataClass = $this->dataClassName;
        if ($dataClass::$noScopeForGuest && ! DatakodeAuth::currentUser(false)) {
            return $this;
        }

        $this->scopes = $scopes;
        return $this;
    }

    public function collect(LengthAwarePaginator|Collection $collection): DataCollection|PaginatedDataCollection
    {
        /** @var DatakodeData $dataClass */
        $dataClass = $this->dataClassName;
        /** @var PaginatedDataCollection|DataCollection $collected */
        $collected = $collection instanceof LengthAwarePaginator ?
            $dataClass::collect($collection, PaginatedDataCollection::class) :
            $dataClass::collect($collection, DataCollection::class);
        return $collected->include(...$dataClass::scopeToInclude($this->scopes));
    }

    public function from(DatakodeApiModel $entity): DatakodeData
    {
        /** @var DatakodeData $dataClass */
        $dataClass = $this->dataClassName;
        return $dataClass::from($entity)->withScopes(...$this->scopes);
    }

    public function getData(
        LengthAwarePaginator|DatakodeApiModel|Collection $collectionOrEntity
    ): DatakodeData|CursorPaginator|Paginator|AbstractCursorPaginator|AbstractPaginator|Collection|Enumerable|LazyCollection|CursorPaginatedDataCollection|DataCollection|PaginatedDataCollection {
        if ($collectionOrEntity instanceof DatakodeApiModel) {
            return $this->from($collectionOrEntity);
        }

        return $this->collect($collectionOrEntity);
    }
}
