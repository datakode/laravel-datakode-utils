<?php

namespace Datakode\LaravelDatakodeUtils\LaravelData;

use Datakode\LaravelDatakodeUtils\Model\Processing;

class ProcessingData extends DatakodeData
{
    protected Processing $entity;

    public function __construct(
        public $hash,
        public $process_class,
        public $url,
        public $name,
        public $filename,
        public $mime_type,
        public $status,
        public $info,
        public $cpt,
        public $max,
        public $percent,
        public $duration,
        public $estimated_duration,
        public $download_url,
        public $mail_me,
        public $mail_sent,
        public $obsolete,
        public $created_at,
        public $started_at,
        public $terminated_at,
        public $downloaded_at,
    ) {
    }
}
