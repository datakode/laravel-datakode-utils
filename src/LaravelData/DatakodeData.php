<?php

/** @noinspection PhpUnused */

namespace Datakode\LaravelDatakodeUtils\LaravelData;

use Datakode\LaravelDatakodeUtils\DatakodeAuth;
use Datakode\LaravelDatakodeUtils\Model\DatakodeApiModel;
use Datakode\LaravelDatakodeUtils\Model\DatakodeUser;
use Illuminate\Support\Collection;
use Override;
use ReflectionClass;
use RuntimeException;
use Spatie\LaravelData\Data;
use Spatie\LaravelData\DataCollection;
use Spatie\LaravelData\Support\Transformation\TransformationContext;
use Spatie\LaravelData\Support\Transformation\TransformationContextFactory;
use UnitEnum;

class DatakodeData extends Data
{
    public static string $dataName = self::class;

    public static bool $removeReadonlyFields = false;

    public static bool $showAllFields = false;

    public static bool $noScopeForGuest = false;

    protected static array $scopes = [];

    /**
     * Fields automatically add as with data if exists on resource
     *
     * @var array|string[]
     */
    protected static array $autoWithFields = ['id', 'k', 'distance_is_less_than'];

    protected ?DatakodeApiModel $object = null;

    /**
     * Extend transform to only keep allowed fields
     */
    #[Override]
    public function transform(
        TransformationContext|TransformationContextFactory|null $transformationContext = null
    ): array {
        $res = parent::transform($transformationContext);
        $keepFields = self::_getFieldsPermissions($res);

        return collect($res)->only($keepFields)
            ->toArray();
    }

    /**
     * Get ressources fields
     */
    public function _getDataRessourceFields(): Collection
    {
        $reflectionClass = new ReflectionClass(static::class);

        // on récupère les champs à partir du constructeur du DataRessource
        return collect($reflectionClass->getConstructor()?->getParameters())
            ->pluck('name');
    }

    /**
     * add data to response
     */
    #[Override]
    public function with(): array
    {
        $res = [];
        if ($this->object) {
            foreach (self::$autoWithFields as $autoWithField) {
                $value = $this->object->getAttribute($autoWithField);

                if ($value !== null) {
                    $res[$autoWithField] = $value;
                }
            }
        }

        return $res;
    }

    /**
     * Override empty to remove hidden fields base on permissions or env
     */
    #[Override]
    public static function empty(array $extra = []): array
    {
        $res = parent::empty($extra);
        $keepFields = self::_getFieldsPermissions($res);

        return [
            'v' => null, // use to clear dropdown in nuxt-datakode-utils
            ...collect($res)
                ->only($keepFields)
                ->toArray(),
        ];
    }

    public static function scoped(UnitEnum ...$scopes): DatakodeDataBag
    {
        return (new DatakodeDataBag(static::class))->setScopes(...$scopes);
    }

    public function withScopes(UnitEnum ...$scope): static
    {
        if (! $scope) {
            return $this;
        }

        foreach ($scope as $item) {
            $this->include(...static::scopeToInclude($item));
        }

        return $this;
    }

    public static function scopeToInclude(UnitEnum|null|array $scope = null): array
    {
        if (! $scope) {
            return [];
        }

        if (is_array($scope)) {
            $includes = [];
            foreach ($scope as $item) {
                $includes = [...$includes, ...self::scopeToInclude($item)];
            }

            return $includes;
        }

        if (isset(static::$scopes[$scope->name])) {
            return static::$scopes[$scope->name];
        }

        if (strtoupper($scope->name) === 'ALL') {
            return ['*'];
        }

        throw new RuntimeException('Scope ' . $scope->name . ' not defined for data model ' . static::class);
    }

    public static function collectWithScopes(Collection $collection, UnitEnum ...$scope): DataCollection
    {
        $collected = self::collect($collection, DataCollection::class);
        $includes = self::scopeToInclude($scope);
        return $collected->include(...$includes);
    }

    /**
     * get field permission collection for this ressource and for current user
     */
    private static function _getFieldsPermissions(array $data): Collection
    {
        // le nom de la ressource utilisé pour les construire les clefs de permission sont basé sur la classe de la DataResouce
        $name = static::$dataName;

        $fields = collect(array_keys($data));

        if (static::$showAllFields) {
            return DatakodeUser::getAvailableFieldsFor($name, $fields);
        }

        $user = DatakodeAuth::currentUser();
        if (! $user) {
            return collect();
        }

        if (static::$removeReadonlyFields) {
            return $user->getOnlyWritableFieldsFor($name, $fields);
        }

        // on filtre pour ne garder que les champs auquel l'utilisateur à access en lecture seule
        return $user->getOnlyReadableFieldsFor($name, $fields);
    }
}
