<?php

namespace Datakode\LaravelDatakodeUtils;

use Datakode\LaravelDatakodeUtils\Model\DatakodeUser;
use Illuminate\Auth\AuthManager;
use Illuminate\Contracts\Auth\Factory as AuthFactory;
use Illuminate\Http\Response;

class DatakodeAuth
{
    public static function currentUser($orGuest = true): ?DatakodeUser
    {
        $model = DatakodeConfig::configOrFail('laravel-datakode-utils.auth.model');
        $allowGuestUser = config('laravel-datakode-utils.auth.allow_guest_user');

        /** @var AuthManager $factory */
        $factory = app(AuthFactory::class);
        /** @var ?DatakodeUser $user */
        $user = $factory->user();

        if (! $user && request()->header('Authorization')) {
            abort(Response::HTTP_UNAUTHORIZED, 'Invalid token');
        }

        if (! $user && $allowGuestUser && $orGuest) {
            return $model::guest();
        }

        return $user;
    }
}
