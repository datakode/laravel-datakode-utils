<?php

namespace Datakode\LaravelDatakodeUtils;

use Illuminate\Config\Repository as ConfigRepository;
use RuntimeException;

class DatakodeConfig
{
    /**
     * Config get but fail if value is null
     */
    public static function configOrFail(?string $key = null): mixed
    {
        $config = app(ConfigRepository::class);
        $res = $config->get($key);
        throw_if($res === null, new RuntimeException("Config var: \"{$key}\" not found !"));
        return $res;
    }
}
