<?php

namespace Datakode\LaravelDatakodeUtils\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class ClearTmpFolder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'LaravelDatakodeUtils:clear-tmp-folder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        collect(Storage::files('tmp'))->each(function (string $file): void {
            //delete file older than 1 hour
            if (time() - Storage::lastModified($file) > 3600) {
                Storage::delete($file);
                $this->info('delete ' . $file);
            }
        });
    }
}
