<?php

namespace Datakode\LaravelDatakodeUtils\Console\Commands;

use Carbon\Carbon;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Spatie\DbDumper\Compressors\GzipCompressor;
use Spatie\DbDumper\Databases\MongoDb;
use Spatie\DbDumper\Databases\MySql;
use Spatie\DbDumper\Databases\PostgreSql;
use Spatie\DbDumper\Databases\Sqlite;
use Symfony\Component\Process\Process;
use function Laravel\Prompts\error;
use function Laravel\Prompts\warning;

class DbDump extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db-dump {dbname?} {--restore} {--timestamped}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dump database';

    private array $errors = [];

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $restore = $this->option('restore');
        $timestamped = $this->option('timestamped');
        $dbname = $this->argument('dbname') ?? Config::string('database.default');

        if (! Arr::has(config('database.connections'), $dbname)) {
            error('Database ' . $dbname . ' not found in config database.connections');
            return;
        }

        $connection = config('database.connections')[$dbname];
        $path = storage_path('db-dump') . ($timestamped ? '/timestamped' : '');
        $baseFilename = $path . '/' . $dbname . ($timestamped ? '-' . Carbon::now()->format('Ymd-His') : '');
        $filename = $baseFilename . '.sql.gz';
        $logFilename = $baseFilename . '.txt';
        if (! File::isDirectory($path)) {
            File::makeDirectory($path);
        }

        if ($restore && ! $timestamped) {
            $path = storage_path('db-dump') . '/restoration_logs/';
            if (! File::isDirectory($path)) {
                File::makeDirectory($path);
            }

            $logFilename = $path . $dbname . '-' . Carbon::now()->format('Ymd-His') . '.txt';
            $this->_restoreDump($connection, $filename, $logFilename);
            return;
        }

        $this->_dumpDb($dbname, $connection, $filename, $logFilename);
    }

    private function bytesToHuman($bytes): string
    {
        $units = ['B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB'];

        for ($i = 0; $bytes > 1024; $i++) {
            $bytes /= 1024;
        }

        return round($bytes, 2) . ' ' . $units[$i];
    }

    public function _dumpDb(string $dbname, mixed $connection, string $filename, string|null $logFilename = null): void
    {
        $logStack = collect();
        $start = Carbon::now();
        $this->info($this->description);
        $this->info('Backup: ' . $dbname);
        $logStack->push('Backup: ' . $dbname);

        $dumper = match ($connection['driver']) {
            'mysql' => MySql::create(),
            'pgsql' => PostgreSql::create(),
            'sqlite' => Sqlite::create(),
            'mongodb' => MongoDb::create(),
            default => throw new Exception('DB driver not handled for dump: ' . $connection['driver']),
        };

        $logStack->push('Driver: ' . $connection['driver']);
        $logStack->push('Database: ' . $connection['database']);
        $logStack->push('File: ' . $filename);

        $dumper
            ->setDbName($connection['database'])
            ->setUserName($connection['username'])
            ->setPassword($connection['password'])
            ->setHost($connection['host'])
            ->setPort($connection['port'])
            ->excludeTables(Arr::get($connection, 'dump_exclude_tables', []))
            ->useCompressor(new GzipCompressor());

        $dumper->dumpToFile($filename);

        $this->info('Dump saved: ' . $filename . ' [' . $this->bytesToHuman(File::size($filename)) . ']');
        $logStack->push('Size: ' . $this->bytesToHuman(File::size($filename)));
        $logStack->push('Start: ' . $start->toIso8601String());
        $logStack->push('End: ' . Carbon::now()->toIso8601String());
        $logStack->push('Duration: ' . $start->diffInSeconds(Carbon::now()) . 's');
        $this->info('Duration: ' . $start->diffInSeconds(Carbon::now()) . 's');

        if ($logFilename) {
            File::put($logFilename, $logStack->implode(PHP_EOL) . PHP_EOL);
        }

    }

    private function _restoreDump(mixed $connection, string $filename, string|null $logFilename = null): void
    {
        $logStack = collect();
        $start = Carbon::now();
        $this->info('Importing database ' . $filename);
        $logStack->push('Importing database: ' . $filename);
        $restoreCommand = match ($connection['driver']) {
            'mysql' => $this->getMysqlRestoreCommand($connection, $filename),
            'pgsql' => $this->getPgsqlRestoreCommand($connection, $filename),
            default => throw new Exception('DB driver not handled for restore: ' . $connection['driver']),
        };

        $logStack->push('Driver: ' . $connection['driver']);
        $logStack->push('Database: ' . $connection['database']);
        $logStack->push('Command: ' . $restoreCommand);
        $logStack->push('Start: ' . $start->toIso8601String());
        $logStack->push('End: ' . Carbon::now()->toIso8601String());
        $logStack->push('Duration: ' . $start->diffInSeconds(Carbon::now()) . 's');

        $log = $this->executeCmd($restoreCommand);
        echo $log;

        $logStack->push(PHP_EOL
            . '===========================' . PHP_EOL
            . '            Log' . PHP_EOL
            . '===========================' . PHP_EOL
            . PHP_EOL);
        $logStack->push($log);

        if ($logFilename) {
            File::put($logFilename, $logStack->implode(PHP_EOL));
        }

        $this->info('Restore done !');
        if ($this->errors !== []) {
            error(implode(PHP_EOL, $this->errors));
            warning('Please check errors !');
        }
    }

    private function getPgsqlRestoreCommand(mixed $connection, string $filename): string
    {
        $res = [];
        $fromPipe = false;
        $fromCompressed = ! str($filename)
            ->endsWith('sql');
        if ($fromCompressed) {
            $fromPipe = true;
            $res[] = $this->_getUncompressCommand($filename);
            $res[] = '|';
        }

        $res = [
            ...$res,
            'PGPASSWORD=' . $connection['password'],
            'psql',
            '-h ' . $connection['host'],
            '-p ' . $connection['port'],
            '-U ' . $connection['username'],
            '-d ' . $connection['database'],
        ];

        if (! $fromPipe) {
            $res[] = '< ' . $filename;
        }

        return collect($res)->implode(' ');
    }

    private function getMysqlRestoreCommand(mixed $connection, string $filename): string
    {
        $res = [];
        $fromPipe = false;
        $fromCompressed = ! str($filename)
            ->endsWith('sql');
        if ($fromCompressed) {
            $fromPipe = true;
            $res[] = $this->_getUncompressCommand($filename);
            $res[] = '|';
        }

        $res = [
            ...$res,
            'mysql',
            '-h ' . $connection['host'],
            '-u ' . $connection['username'],
            '-p ' . $connection['password'],
            '-P ' . $connection['port'],
            $connection['database'],
        ];

        if (! $fromPipe) {
            $res[] = '< ' . $filename;
        }

        return collect($res)->implode(' ');
    }

    private function _getUncompressCommand(string $filename): string
    {
        return match (File::extension($filename)) {
            'gz' => "gunzip -c {$filename}",
            'bz2' => "bunzip2 -c {$filename}",
            default => throw new Exception('Unknown compression format for dump: ' . $filename),
        };
    }

    private function executeCmd(string $cmd): string
    {
        $process = Process::fromShellCommandline($cmd);

        $processOutput = '';

        $captureOutput = function ($type, string $line) use (&$processOutput): void {
            $processOutput .= $line;
            if (str($line)->startsWith('ERROR:')) {
                $this->errors[] = $line;
            }

        };

        $process->setTimeout(null)
            ->run($captureOutput);

        if ($process->getExitCode()) {
            $exception = new Exception($cmd . ' - ' . $processOutput);
            report($exception);

            throw $exception;
        }

        return $processOutput;
    }
}
