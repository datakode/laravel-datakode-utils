<?php

/** @noinspection PhpUnused */

namespace Datakode\LaravelDatakodeUtils;

use Carbon\Carbon;
use Exception;
use Illuminate\Support\Str;

class DatakodeHelper
{
    /**
     * parse boolean value
     */
    public static function booleanValue(mixed $value): bool|null
    {
        if (is_string($value)) {
            $value = Str::lower($value);
            if (in_array($value, ['vrai', 'oui', 'yes', 'true'])) {
                return true;
            }

            if (in_array($value, ['faux', 'non', 'no', 'false'])) {
                return false;
            }
        }

        return is_string($value) ? filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) : (bool) $value;
    }

    /**
     * Parse date value
     */
    public static function dateValue(mixed $value): Carbon|null
    {
        if ($value === null) {
            return null;
        }

        if (is_int($value) || is_float($value)) {
            if ($value < 1000000) {
                // excel date format
                return Carbon::createFromTimestamp(0)->subYears(70)->subDays(2)->addDays($value)->startOfDay();
            }

            return Carbon::createFromTimestamp($value)->startOfDay();
        }

        try {
            $date = Carbon::createFromIsoFormat('l', $value, null, 'fr')->startOfDay();
            if ($date->year >= 1000) {
                return $date;
            }

            return Carbon::createFromIsoFormat('DD/MM/YY', $value)->startOfDay();
        } catch (Exception) {
        }

        return Carbon::parse((string) $value)->startOfDay();
    }

    /**
     * Return secret value from docker mounted file
     * return env value with default if app isLocal and $returnEnvOnLocal is true
     */
    public static function getDockerSecret(
        string $key,
        mixed $default_local_only = null,
        bool $returnEnvOnLocal = true
    ): string {
        $isLocal = env('APP_ENV') === 'local';
        $isTesting = env('APP_ENV') === 'testing';
        $filename = '/run/secrets/' . $key;
        if (! file_exists($filename)) {
            if ($isTesting || ($isLocal && $returnEnvOnLocal)) {
                return env($key, $default_local_only);
            }

            throw new Exception("Secret file does not exists: {$filename} [env: " . env('APP_ENV') . ']');
        }

        $secret = trim(file_get_contents($filename));

        if (! $isLocal && ! $isTesting) {
            // check if secret is secure
            $insecure = ['', 'secret', 'changeme', 'pwd', 'passwd', 'password'];
            $secret_slugged = strtolower($secret);
            $secret_slugged = preg_replace('/[^a-zA-Z]/i', '', $secret_slugged);

            if (in_array($secret_slugged, $insecure)) {
                throw new Exception(
                    "Secret file, secure value must be set: {$filename} (value found:\"{$secret_slugged}\")"
                );
            }
        }

        return $secret;
    }
}
