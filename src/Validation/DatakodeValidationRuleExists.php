<?php

namespace Datakode\LaravelDatakodeUtils\Validation;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Illuminate\Support\Traits\Conditionable;
use Illuminate\Validation\Rules\DatabaseRule;
use Illuminate\Validation\Rules\Exists;

class DatakodeValidationRuleExists extends Exists
{
    use Conditionable;
    use DatabaseRule;

    private ?string $_autocompleteRoute = null;

    private array $_autocompleteRouteFilter = [];

    public function setAutocompleteRoute(?string $autocompleteRoute, array $filter = []): self
    {
        $this->_autocompleteRoute = $autocompleteRoute;
        $this->_autocompleteRouteFilter = $filter;

        return $this;
    }

    /**
     * @noinspection PhpUnused
     */
    public function toMetaString(): string
    {
        $res = $this->__toString();

        return $res . '|autocompleteUrl:' . $this->getAutocompleteUrl();
    }

    public function getAutocompleteUrl(): ?string
    {
        $routeName = $this->_autocompleteRoute ?? $this->table;
        $routeParameters = [
            'filter' => $this->_autocompleteRouteFilter,
            'format' => 'list',
        ];
        if (Route::has($routeName)) {
            return Str::of(route($routeName, $routeParameters))->replaceStart(route('api.home') . '/', '');
        }

        if (Route::has($routeName . '.index')) {
            return Str::of(route($routeName . '.index', $routeParameters))->replaceStart(route('api.home') . '/', '');
        }

        return $this->_autocompleteRoute;
    }
}
