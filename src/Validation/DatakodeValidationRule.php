<?php

namespace Datakode\LaravelDatakodeUtils\Validation;

use Datakode\LaravelDatakodeUtils\Rules\DatakodeValidationRuleAlwaysFailInfo;
use Illuminate\Validation\Rule;
use Override;

class DatakodeValidationRule extends Rule
{
    #[Override]
    public static function exists($table, $column = 'NULL'): DatakodeValidationRuleExists
    {
        return new DatakodeValidationRuleExists($table, $column);
    }

    public static function info(string $string): DatakodeValidationRuleInfo
    {
        return new DatakodeValidationRuleInfo($string);
    }

    public static function alwaysFailInfo(string $string): DatakodeValidationRuleAlwaysFailInfo
    {
        return new DatakodeValidationRuleAlwaysFailInfo($string);
    }

    public static function jsonInfo(mixed $array): DatakodeValidationRuleJsonInfo
    {
        return new DatakodeValidationRuleJsonInfo($array);
    }

    public static function existsMultiple($table, $column = 'NULL'): DatakodeValidationRuleExistsMultiple
    {
        return new DatakodeValidationRuleExistsMultiple($table, $column);
    }

    public static function noFrontValidation(): DatakodeValidationNoFrontValidation
    {
        return new DatakodeValidationNoFrontValidation();
    }
}
