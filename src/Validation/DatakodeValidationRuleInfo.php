<?php

namespace Datakode\LaravelDatakodeUtils\Validation;

use Override;
use Stringable;

class DatakodeValidationRuleInfo implements Stringable
{
    public function __construct(
        protected string $info
    ) {
    }

    #[Override]
    public function __toString(): string
    {
        return '';
    }

    /**
     * @noinspection PhpUnused
     */
    public function toMetaString(): string
    {
        return "info:{$this->info}";
    }
}
