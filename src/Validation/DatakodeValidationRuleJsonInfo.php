<?php

namespace Datakode\LaravelDatakodeUtils\Validation;

use Override;
use Stringable;

class DatakodeValidationRuleJsonInfo implements Stringable
{
    public function __construct(
        protected array $json_info
    ) {
    }

    #[Override]
    public function __toString(): string
    {
        return '';
    }

    /**
     * @noinspection PhpUnused
     */
    public function toMetaString(): string
    {
        return 'json_info:' . json_encode($this->json_info);
    }
}
