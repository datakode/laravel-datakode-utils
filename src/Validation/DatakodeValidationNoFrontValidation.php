<?php

namespace Datakode\LaravelDatakodeUtils\Validation;

use Override;
use Stringable;

class DatakodeValidationNoFrontValidation implements Stringable
{
    #[Override]
    public function __toString(): string
    {
        return '';
    }

    /**
     * @noinspection PhpUnused
     */
    public function toMetaString(): string
    {
        return 'noFrontValidation';
    }
}
