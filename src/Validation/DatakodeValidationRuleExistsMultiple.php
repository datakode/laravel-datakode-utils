<?php

namespace Datakode\LaravelDatakodeUtils\Validation;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Illuminate\Support\Traits\Conditionable;
use Illuminate\Validation\Rules\DatabaseRule;
use Override;
use Stringable;

class DatakodeValidationRuleExistsMultiple implements Stringable
{
    use Conditionable;
    use DatabaseRule;

    #[Override]
    public function __toString(): string
    {
        return '';
    }

    private ?string $_autocompleteRoute = null;

    private array $_autocompleteRouteFilter = [];

    public function setAutocompleteRoute(?string $autocompleteRoute, array $filter = []): self
    {
        $this->_autocompleteRoute = $autocompleteRoute;
        $this->_autocompleteRouteFilter = $filter;

        return $this;
    }

    public function getAutocompleteUrl(): ?string
    {
        $routeName = $this->_autocompleteRoute ?? $this->table;
        $routeParameters = [
            'filter' => $this->_autocompleteRouteFilter,
            'format' => 'list',
        ];
        if (Route::has($routeName)) {
            return Str::of(route($routeName, $routeParameters))->replaceStart(route('api.home') . '/', '');
        }

        if (Route::has($routeName . '.index')) {
            return Str::of(route($routeName . '.index', $routeParameters))->replaceStart(route('api.home') . '/', '');
        }

        return $this->_autocompleteRoute;
    }

    /**
     * @noinspection PhpUnused
     */
    public function toMetaString(): string
    {
        return 'array|existsMultiple|autocompleteUrl:' . $this->getAutocompleteUrl();
    }
}
