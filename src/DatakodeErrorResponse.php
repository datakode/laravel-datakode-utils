<?php

/** @noinspection PhpUnused */

namespace Datakode\LaravelDatakodeUtils;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class DatakodeErrorResponse
{
    public function __construct(
        protected Exception $exception
    ) {
    }

    public function toResponse(): JsonResponse|null
    {
        $exception = $this->exception;

        if ($exception instanceof ValidationException) {
            // keep original error response
            return null;
        }

        $translateMessage = $this->translate($exception->getMessage());

        $statusCode = $this->getStatusCode($exception);

        $response = [
            'code' => $statusCode,
            'status' => Arr::get(Response::$statusTexts, $statusCode, '?'),
            'exception' => class_basename($exception::class),
            'exception_class' => $exception::class,
            'message' => $translateMessage,
            'message_ori' => $exception->getMessage(),
        ];

        if (config('app.debug')) {
            $response['debug'] = [
                'file' => $exception->getFile(),
                'line' => $exception->getLine(),
                'error_code' => $exception->getCode(),
                'stack_trace' => explode("\n", $exception->getTraceAsString()),
            ];

        }

        return response()->json($response, $statusCode);
    }

    protected function translate(string $message): string
    {
        $translateMessage = trans($message);
        if ($translateMessage !== $message) {
            return $translateMessage;
        }

        $this->_translateExploded($message);
        $this->_translateWithoutEnd($message);
        $this->_translateWithoutStart($message);
        return $message;
    }

    public function getStatusCode(Exception $exception): mixed
    {
        if ($exception instanceof AuthenticationException) {
            return Response::HTTP_UNAUTHORIZED;
        }

        return method_exists(
            $exception,
            'getStatusCode'
        ) ? $exception->getStatusCode() : $exception->status ?? Response::HTTP_INTERNAL_SERVER_ERROR;
    }

    private function _translateExploded(string &$message): void
    {
        foreach ([':', '[', ']', '. ', '`', ', '] as $separator) {
            if (Str::contains($message, $separator)) {
                $message = Str::of($message)->explode($separator)->map(function ($item): string {
                    return $this->translate($item);
                })->implode($separator);
            }
        }
    }

    private function _translateWithoutEnd(string &$message): void
    {
        foreach ([' ', '.'] as $end) {
            if (Str::endsWith($message, $end)) {
                $message = $this->translate(substr($message, 0, -(strlen($end)))) . $end;
            }
        }
    }

    private function _translateWithoutStart(string &$message): void
    {
        foreach ([' ', '-'] as $start) {
            if (Str::startsWith($message, $start)) {
                $message = $start . $this->translate(substr($message, strlen($start)));
            }
        }
    }
}
