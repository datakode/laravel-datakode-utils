<?php

namespace Datakode\LaravelDatakodeUtils\Notifications;

use Datakode\LaravelDatakodeUtils\LaravelData\ProcessingData;
use Datakode\LaravelDatakodeUtils\Model\DatakodeUser;
use Datakode\LaravelDatakodeUtils\Model\Processing;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Str;

class ProcessingEndNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     */
    public function __construct(
        public Processing $processing
    ) {
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(DatakodeUser $notifiable): array
    {
        return config('laravel-datakode-utils.processing.via', ['mail']);
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        $processing = $this->processing;
        $subject = trans(
            'laravel-datakode-utils::laravel-datakode-utils.process_end_email_subject.' . Str::of(
                class_basename($processing->process_class)
            )->snake()
        );

        $processing->mail_sent = true;
        $processing->save();

        return (new MailMessage())
            ->subject($subject)
            ->line(Str::of($subject)->trim('.') . '.')
            ->action(
                trans('laravel-datakode-utils::laravel-datakode-utils.process_end_email_title'),
                config('laravel-datakode-utils.processing.webapp_download_url') . $processing->hash
            );
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return ProcessingData::from($this->processing)->toArray();
    }
}
