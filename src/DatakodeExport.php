<?php

namespace Datakode\LaravelDatakodeUtils;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\DefaultValueBinder;
use Override;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;

class DatakodeExport extends DefaultValueBinder implements FromCollection, WithCustomValueBinder, WithHeadings, WithStrictNullComparison
{
    private Collection $data;

    public function __construct(
        array $data,
        protected bool $translateHeading = true,
        protected string|null $translate_key = 'exports'
    ) {
        $data = collect($data)
            ->map(function ($line) {
                return Arr::dot($line);
            })->toArray();

        $this->data = collect($data);
    }

    #[Override]
    public function collection(): Collection
    {
        return $this->data;
    }

    #[Override]
    public function headings(): array
    {
        return collect(array_keys($this->data[0]))
            ->map(function (string $k) {
                if (! $this->translateHeading || $this->translate_key === null) {
                    return $k;
                }

                $translationKey = "{$this->translate_key}.{$k}";
                $translation = trans($translationKey);

                return ($translation === $translationKey) ? $k : $translation;
            })->toArray();
    }

    #[Override]
    public function bindValue(Cell $cell, $value): bool
    {
        // skip default behavior for string
        if (is_string($value)) {
            // force string to be typed as string
            $cell->setValueExplicit($value, DataType::TYPE_STRING);

            return true;
        }

        // else return default behavior
        return parent::bindValue($cell, $value);
    }
}
