<?php

namespace Datakode\LaravelDatakodeUtils;

use Exception;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Password;
use RuntimeException;

class DatakodeHealthCheck
{
    private static array $tests = [];

    public function checkDB(): self
    {
        $this->_pushTest('check DB access');
        DB::connection()->getPDO();
        DB::connection()->getDatabaseName();
        return $this;
    }

    public function checkTableExists(string ...$tables): self
    {
        foreach ($tables as $table) {
            $this->_pushTest('check Table exist: "' . $table . '"');
            $this->getTableRowCount($table);
        }

        return $this;
    }

    public function checkTableExistsAndNotEmpty(string ...$tables): self
    {
        foreach ($tables as $table) {
            $this->_pushTest('check Table exists and is not empty: "' . $table . '"');
            $count = $this->getTableRowCount($table);
            if ($count == 0) {
                throw new RuntimeException('Table "' . $table . '" is empty');
            }
        }

        return $this;
    }

    public function checkConfigPasswordSecurity(string $configKey, Rule $rules = null): self
    {
        $this->_pushTest('check password security: " config(\'' . $configKey . '\')"');
        if (! $rules) {
            $rules = Password::min(8)
                ->mixedCase()
                ->numbers()
                ->letters()
//                ->symbols()
                ->uncompromised();
        }

        $secret = config($configKey);
        $validator = Validator::make([
            'secret' => $secret,
        ], [
            'secret' => $rules,
        ]);
        if ($validator->fails()) {
            throw new RuntimeException('Password "config(\'' . $configKey . '\')" is insecure [' .
                implode(' ', $validator->errors()->toArray()['secret'])
                . ']');
        }

        return $this;
    }

    public function checkCache(): self
    {
        $this->_pushTest('check cache access');
        try {
            $testVal = Str::random();
            Cache::put('testVal', $testVal);
            $foundVal = Cache::get('testVal');
        } catch (Exception) {
            throw new RuntimeException('Cache error !');
        }

        if ($foundVal != $testVal) {
            throw new RuntimeException('Cache error !');
        }

        return $this;
    }

    // TODO check storage is writable

    // TODO ... other health checks

    public static function getTestsResults(): array
    {
        return array_map(fn ($test): string => $test . ' OK', self::$tests);
    }

    public function getTableRowCount(string $table): int
    {
        try {
            $count = DB::select('SELECT COUNT(*) FROM "' . $table . '"');
        } catch (Exception) {
            throw new RuntimeException('Table "' . $table . '" not found');
        }

        return $count[0]?->count;
    }

    private function _pushTest(string $string): void
    {
        self::$tests[] = $string;
    }
}
