<?php

namespace Datakode\LaravelDatakodeUtils\Import;

use Illuminate\Contracts\Validation\Factory;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException as IlluminateValidationException;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Exceptions\RowSkippedException;
use Maatwebsite\Excel\Validators\RowValidator as BaseRowValidatorAlias;
use Maatwebsite\Excel\Validators\ValidationException;

class ByRowValidator extends BaseRowValidatorAlias
{
    private readonly Factory $validationFactory;

    public function __construct(Factory $validator)
    {
        parent::__construct($validator);
        $this->validationFactory = $validator;
    }

    public function validateRow(array $rows, WithValidation $import, array $rules, $rowNumber): void
    {
        $messages = $this->messages($import);
        $attributes = $this->attributes($import);

        try {
            $validator = $this->validationFactory->make($rows, $rules, $messages, $attributes);
            $validator->validate();
        } catch (IlluminateValidationException $illuminateValidationException) {
            $failures = [];
            foreach ($illuminateValidationException->errors() as $attribute => $errors) {
                $row = $rowNumber;
                $attributeName = $attribute;
                $failures[] = new ByRowFailure($row, $attributeName, $errors, $rows[$row] ?? []);
            }

            if ($import instanceof SkipsOnFailure) {
                $import->onFailure(...$failures);
                throw new RowSkippedException(...$failures);
            }

            throw new ValidationException($illuminateValidationException, $failures);
        }
    }

    private function messages(WithValidation $import): array
    {
        return method_exists($import, 'customValidationMessages')
            ? $this->formatKey($import->customValidationMessages())
            : [];
    }

    private function attributes(WithValidation $import): array
    {
        return method_exists($import, 'customValidationAttributes')
            ? $this->formatKey($import->customValidationAttributes())
            : [];
    }

    private function formatKey(array $elements): array
    {
        return collect($elements)->mapWithKeys(function ($rule, $attribute): array {
            $attribute = Str::startsWith($attribute, '*.') ? $attribute : '*.' . $attribute;

            return [
                $attribute => $this->formatRule($rule),
            ];
        })->all();
    }

    /**
     * @param  string|object|callable|array  $rules
     * @return string|array
     */
    private function formatRule($rules)
    {
        if (is_array($rules)) {
            foreach ($rules as $rule) {
                $formatted[] = $this->formatRule($rule);
            }

            return $formatted ?? [];
        }

        if (is_object($rules) || is_callable($rules)) {
            return $rules;
        }

        if (Str::contains($rules, 'required_') && preg_match('/(.*?):(.*),(.*)/', $rules, $matches)) {
            $column = Str::startsWith($matches[2], '*.') ? $matches[2] : '*.' . $matches[2];

            return $matches[1] . ':' . $column . ',' . $matches[3];
        }

        return $rules;
    }
}
