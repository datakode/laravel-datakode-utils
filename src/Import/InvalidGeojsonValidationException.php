<?php

/** @noinspection PhpUnused */

namespace Datakode\LaravelDatakodeUtils\Import;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class InvalidGeojsonValidationException extends ImportValidationException
{
    public function __construct(
        public $reason = '',
        public $errors = []
    ) {
        parent::__construct('invalid geojson');
    }

    /**
     * Render the exception into an HTTP response.
     */
    public function render(Request $request): Response
    {
        return new Response($this->summarize(), 422);
    }

    private function message(): string
    {
        return __('The given data was invalid.') . ' Fichier geojson invalide.' . ($this->reason ? ' [' . $this->reason . ']' : '');
    }

    private function summarize(): array
    {
        $message = $this->message();
        $errors = $this->errors;
        return compact('message', 'errors');
    }
}
