<?php

/** @noinspection PhpUnused */

namespace Datakode\LaravelDatakodeUtils\Import;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class NotWritableFieldValidationException extends ImportValidationException
{
    public function __construct(
        public array $unknownFields
    ) {
        parent::__construct('unknown fields');
    }

    /**
     * Render the exception into an HTTP response.
     */
    public function render(Request $request): Response
    {
        return new Response($this->summarize(), 422);
    }

    private function message()
    {
        $messages = $this->errors();

        if (! count($messages) || ! is_string($messages[0])) {
            return __('The given data was invalid.');
        }

        $message = array_shift($messages);

        if ($count = count($messages)) {
            $pluralized = $count === 1 ? 'error' : 'errors';

            $message .= ' ' . trans_choice("(and :count more {$pluralized})", $count, compact('count'));
        }

        return $message;
    }

    private function errors()
    {
        return collect($this->unknownFields)->map(function ($field) {
            return trans('laravel-datakode-utils::laravel-datakode-utils.not-writable-fields-error', compact('field'));
        })->values()
            ->toArray();
    }

    private function summarize(): array
    {
        $message = $this->message();
        $errors = $this->errors();
        return compact('message', 'errors');
    }
}
