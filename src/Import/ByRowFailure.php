<?php

namespace Datakode\LaravelDatakodeUtils\Import;

use Illuminate\Support\Str;
use Maatwebsite\Excel\Validators\Failure;
use Override;

class ByRowFailure extends Failure
{
    /**
     * @return array
     */
    #[Override]
    public function toArray()
    {
        return collect($this->errors)->map(function ($message) {
            $message = __('There was an error on row :row. :message', [
                'row' => $this->row,
                'message' => $message,
            ]);
            $message = Str::of($message);
            $message = $message->replace('0', 'O'); // TODO find better fix to disable 0 removed by ErrorHandler
            // TODO find a way to not add Erreur ligne 0.

            return $message->toString();
        })->all();
    }
}
