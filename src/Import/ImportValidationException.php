<?php

/** @noinspection PhpUnused */

namespace Datakode\LaravelDatakodeUtils\Import;

use Exception;

abstract class ImportValidationException extends Exception
{
}
