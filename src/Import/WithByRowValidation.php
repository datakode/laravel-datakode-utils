<?php

namespace Datakode\LaravelDatakodeUtils\Import;

interface WithByRowValidation
{
    public function rulesForRow(array $row): array;
}
