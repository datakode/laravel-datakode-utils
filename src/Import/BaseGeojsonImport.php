<?php

/** @noinspection PhpUnused */

namespace Datakode\LaravelDatakodeUtils\Import;

use Datakode\LaravelDatakodeUtils\DatakodeExport;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use InvalidArgumentException;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;
use MatanYadaev\EloquentSpatial\Enums\Srid;
use MatanYadaev\EloquentSpatial\Objects\Geometry;
use Override;

abstract class BaseGeojsonImport extends BaseImport implements WithValidation
{
    #[Override]
    public function dryImport(): static
    {
        return $this->import(true);
    }

    #[Override]
    public function import(bool $dry = false): static
    {

        $geojsonData = json_decode($this->file->getContent(), true);
        $data = $this->getDataFromGeojson($geojsonData);

        // create tmp csv file from data
        Storage::makeDirectory('tmp');
        $tmpFilePath = 'tmp/' . md5('geojson_export' . time()) . '.csv';
        Excel::store(new DatakodeExport($data), $tmpFilePath, null, \Maatwebsite\Excel\Excel::CSV);

        HeadingRowFormatter::default('custom');
        $this->dry = $dry;
        $this->_reader = Excel::import($this, storage_path('app/' . $tmpFilePath));
        return $this;

    }

    #[Override]
    public function prepareForValidation(array $data)
    {
        $res = parent::prepareForValidation($data);
        return $this->geomToObject($res);
    }

    public function getDataFromGeojson(array $geojsonData): array
    {
        $model = new (static::getModelClass());

        if (! Arr::has($geojsonData, ['type', 'features'])) {
            throw new InvalidGeojsonValidationException('Geojson data must contain : type, geometry');
        }

        return collect($geojsonData['features'])->map(function (array $feature) use ($model) {
            if (! Arr::has($feature, ['properties', 'geometry', 'geometry.coordinates'])) {
                throw new InvalidGeojsonValidationException(
                    'Geojson features data must contain: properties, geometry, geometry.coordinates'
                );
            }

            $this->_checkCoordinatesValidity($feature['geometry']['coordinates'], $model);

            $res = $feature['properties'];
            $res['geometry'] = json_encode($feature['geometry']);
            return $res;
        })->toArray();

    }

    protected function geomToObject(array $res): array
    {
        $model = new (static::getModelClass());
        if (! $res['geometry']) {
            throw new InvalidGeojsonValidationException([]);
        }

        try {
            $res['geometry'] = Geometry::fromJson($res['geometry'], $model->srid);
        } catch (InvalidArgumentException) {
            throw new InvalidGeojsonValidationException([]);
        }

        return $res;
    }

    private function _checkCoordinatesValidity($coordinates, mixed $model): void
    {
        collect(Arr::dot($coordinates))
            ->map(function (float $coordinate, $k) use ($model): void {
                $kLast = Str::of((string) $k)->explode('.')->last();
                $this->_checkCoords($model->srid, $kLast, $coordinate);

            });
    }

    public function _checkCoords(Srid $srid, string $coordinateType, float $value): void
    {

        $params = [
            // https://epsg.io/3857
            Srid::WGS84->name => [
                'name' => [
                    '0' => 'longitude',
                    '1' => 'latitude',
                ],
                '0' => [-180, 180],
                '1' => [-90, 90],
            ],
            // https://epsg.io/3857
            Srid::WEB_MERCATOR->name => [
                'name' => [
                    '0' => 'X',
                    '1' => 'Y',
                ],
                '0' => [-20037508.34, 20037508.34],
                '1' => [-20048966.1, 20048966.1],
            ],
        ];

        $limits = $params[$srid->name];

        if ($value < $limits[$coordinateType][0] || $value > $limits[$coordinateType][1]) {
            throw new InvalidGeojsonValidationException(
                "La projection attendue est {$srid->name} (EPSG:{$srid->value}). Les coordonnées du Geojson {$limits['name'][$coordinateType]} doit être entre {$limits[$coordinateType][0]} et {$limits[$coordinateType][1]}",
                ["Coordonnée {$limits['name'][$coordinateType]} trouvée : {$value}"]
            );
        }
    }
}
