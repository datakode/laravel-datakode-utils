<?php

/** @noinspection PhpUnused */

namespace Datakode\LaravelDatakodeUtils\Import;

use Datakode\LaravelDatakodeUtils\DatakodeAuth;
use Datakode\LaravelDatakodeUtils\DatakodeHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Bus\PendingDispatch;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;
use Maatwebsite\Excel\Reader;
use Maatwebsite\Excel\Validators\ValidationException as ExcelValidationException;
use Override;

abstract class BaseImport implements ToCollection, WithValidation, WithHeadingRow
{
    protected bool $dry = false;

    private array $_log = [];

    protected \Maatwebsite\Excel\Excel|Reader|PendingDispatch $_reader;

    public function __construct(
        public File|UploadedFile|null $file = null,
    ) {
    }

    public static function escapeDot(string $value)
    {
        return Str::replace('.', '_DOT_', $value);
    }

    public static function unescapeDot(string $value)
    {
        return Str::replace('_DOT_', '.', $value);
    }

    public function dryImport(): static
    {
        return $this->import(true);
    }

    public function import(bool $dry = false): static
    {
        HeadingRowFormatter::default('custom');
        $this->dry = $dry;
        $this->_reader = Excel::import($this, $this->file);
        return $this;
    }

    #[Override]
    public function collection(Collection $collection): void
    {
        foreach ($collection as $row) {
            if (! $this->dry) {
                $row = $this->unDotData($row->toArray());
                $this->saveModel($row);
            }
        }
    }

    public function withValidator($validator): void
    {
        $withRowValidation = $this instanceof WithByRowValidation && method_exists($this, 'rulesForRow');
        if ($withRowValidation) {
            $data = $validator->getData();
            $failures = [];
            foreach ($data as $k => $row) {
                $row = $this->unDotData($row);
                $rules = $this->rulesForRow($row);
                try {
                    app(ByRowValidator::class)->validateRow($row, $this, $rules, $k);
                } catch (ExcelValidationException $e) {
                    $failures = [...$failures, ...$e->failures()];
                }
            }

            if ($failures !== []) {
                throw new ExcelValidationException(new ValidationException($validator), $failures);
            }
        }
    }

    /**
     * @return Model|Model[]|null
     */
    abstract public function saveModel(array $row): Model|array|null;

    abstract public static function getModelClass(): string;

    #[Override]
    abstract public function rules(): array;

    public function prepareForValidation(array $data)
    {
        $this->removeEmptyRows($data);
        $this->castBooleans($data);
        $this->castArrays($data);
        $this->castDates($data);

        $withRejectNotInRules = $this instanceof WithRejectNotInRules;
        if ($withRejectNotInRules) {
            $this->rejectIfNotInRules($data);
        }

        $this->rejectIfNotWritable($data);

        return $data;
    }

    public function log(string $stack, array $data): void
    {
        if (! isset($this->_log[$stack])) {
            $this->_log[$stack] = [];
        }

        $this->_log[$stack][] = $data;
    }

    public function setLog(array $log): void
    {
        $this->_log = $log;
    }

    public function getLog(): array
    {
        return $this->_log;
    }

    public function getReader(): PendingDispatch|Reader|\Maatwebsite\Excel\Excel
    {
        return $this->_reader;
    }

    protected function removeEmptyRows(array &$data): static
    {
        $data = collect($data)
            ->filter(function ($value, $key): bool {
                if ($key === '') {
                    return false;
                }

                return $value !== null || ! is_int($key);
            })->toArray();

        return $this;
    }

    protected function castBooleans(array &$data): static
    {
        $booleanRules = $this->getRulesHavingRule('boolean');

        foreach ($booleanRules as $booleanRule) {
            $booleanRule = self::escapeDot($booleanRule);
            if (isset($data[$booleanRule])) {
                $data[$booleanRule] = DatakodeHelper::booleanValue($data[$booleanRule]);
            }
        }

        return $this;
    }

    protected function castArrays(array &$data): static
    {
        $arrayRules = $this->getRulesHavingRule('array');

        foreach ($arrayRules as $arrayRule) {
            $arrayRule = self::escapeDot($arrayRule);
            if (isset($data[$arrayRule])) {
                $data[$arrayRule] = explode(',', (string) $data[$arrayRule]);
            }
        }

        return $this;
    }

    protected function castDates(array &$data): static
    {
        $dateRules = $this->getRulesHavingRule('date');
        foreach ($dateRules as $dateRule) {
            $dateRule = self::escapeDot($dateRule);
            if (isset($data[$dateRule])) {
                $data[$dateRule] = DatakodeHelper::dateValue($data[$dateRule]);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    protected function getRulesHavingRule(string $rule)
    {
        return collect($this->rules())
            ->filter(function ($v) use ($rule): bool {
                if (is_string($v)) {
                    $v = explode('|', $v);
                }

                return in_array($rule, $v);
            })
            ->keys()
            ->toArray();
    }

    protected function rejectIfNotInRules(array $data)
    {
        $data = $this->unDotData($data);
        $rulesKey = collect($this->rules())
            ->keys()
            ->toArray();
        $unknownFields = collect($data)
            ->keys()
            ->filter(function ($k) use ($rulesKey): bool {
                return ! in_array($k, $rulesKey);
            });
        if ($unknownFields->count()) {
            throw new UnknownFieldValidationException($unknownFields->toArray());
        }
    }

    protected function unDotData(array $data): array
    {
        return collect($data)
            ->mapWithKeys(function ($v, $k): array {
                $k = self::unescapeDot($k);
                return [
                    $k => $v,
                ];
            })->toArray();
    }

    protected function rejectIfNotWritable(array $data): void
    {
        $data = $this->unDotData($data);
        $user = DatakodeAuth::currentUser();
        $notWritableFields = collect($data)
            ->keys()
            ->filter(function ($k) use ($user): bool {
                return ! $user->hasWriteAccessToField(static::getModelClass(), $k);
            });
        if ($notWritableFields->count()) {
            throw new NotWritableFieldValidationException($notWritableFields->toArray());
        }
    }
}
