<?php

namespace Datakode\LaravelDatakodeUtils\Rules;

use MatanYadaev\EloquentSpatial\Objects\MultiLineString;

class GeometryMultiLineString extends Geometry
{
    protected ?string $requiredObjectClass = MultiLineString::class;
}
