<?php

namespace Datakode\LaravelDatakodeUtils\Rules;

use MatanYadaev\EloquentSpatial\Objects\GeometryCollection;

class GeometryGeometryCollection extends Geometry
{
    protected ?string $requiredObjectClass = GeometryCollection::class;
}
