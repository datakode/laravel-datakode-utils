<?php

namespace Datakode\LaravelDatakodeUtils\Rules;

use MatanYadaev\EloquentSpatial\Objects\MultiPolygon;

class GeometryMultiPolygon extends Geometry
{
    protected ?string $requiredObjectClass = MultiPolygon::class;
}
