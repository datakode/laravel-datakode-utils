<?php

namespace Datakode\LaravelDatakodeUtils\Rules;

use MatanYadaev\EloquentSpatial\Objects\LineString;

class GeometryLineString extends Geometry
{
    protected ?string $requiredObjectClass = LineString::class;
}
