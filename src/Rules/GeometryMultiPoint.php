<?php

namespace Datakode\LaravelDatakodeUtils\Rules;

use MatanYadaev\EloquentSpatial\Objects\MultiPoint;

class GeometryMultiPoint extends Geometry
{
    protected ?string $requiredObjectClass = MultiPoint::class;
}
