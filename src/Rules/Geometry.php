<?php

namespace Datakode\LaravelDatakodeUtils\Rules;

use Closure;
use Illuminate\Support\Arr;
use MatanYadaev\EloquentSpatial\Objects\Geometry as GeometryObject;
use Override;

class Geometry extends BaseRule
{
    protected ?string $requiredObjectClass = null;

    #[Override]
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if ($this->requiredObjectClass) {
            $this->validateGeomClass($value, $fail);
            return;
        }

        if (is_array($value)) {
            return;
        }

        if (is_subclass_of($value, GeometryObject::class)) {
            return;
        }

        $fail('The :attribute type is not a child of GeometryObject.');
    }

    private function validateGeomClass(mixed $value, Closure $fail): void
    {
        $className = class_basename($this->requiredObjectClass);
        if (! is_array($value)) {
            if (! ($value instanceof $this->requiredObjectClass)) {
                $fail("The :attribute type not a {$className}.");
            }

            return;
        }

        if (Arr::get($value, 'type') !== $className) {
            $fail("The :attribute type must be {$className}.");
        }
    }
}
