<?php

namespace Datakode\LaravelDatakodeUtils\Rules;

use MatanYadaev\EloquentSpatial\Objects\Polygon;

class GeometryPolygon extends Geometry
{
    protected ?string $requiredObjectClass = Polygon::class;
}
