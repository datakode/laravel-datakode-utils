<?php

namespace Datakode\LaravelDatakodeUtils\Rules;

use MatanYadaev\EloquentSpatial\Objects\Point;

class GeometryPoint extends Geometry
{
    protected ?string $requiredObjectClass = Point::class;
}
