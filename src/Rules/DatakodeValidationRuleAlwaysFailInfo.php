<?php

namespace Datakode\LaravelDatakodeUtils\Rules;

use Closure;
use Override;

class DatakodeValidationRuleAlwaysFailInfo extends BaseRule
{
    public function __construct(
        protected string $info
    ) {
    }

    /**
     * @noinspection PhpUnused
     */
    public function toMetaString(): string
    {
        return "alwaysFail|info:{$this->info}";
    }

    #[Override]
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $fail($this->info);
    }
}
