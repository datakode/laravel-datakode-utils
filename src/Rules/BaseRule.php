<?php

namespace Datakode\LaravelDatakodeUtils\Rules;

use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Support\Str;
use Override;
use Stringable;

abstract class BaseRule implements Stringable, ValidationRule
{
    #[Override]
    public function __toString(): string
    {
        return self::handle();
    }

    public static function handle(): string
    {
        return Str::snake(class_basename(static::class));
    }
}
