<?php

namespace Datakode\LaravelDatakodeUtils;

use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Override;
use Stringable;

class DatakodeApiResponse implements Stringable
{
    protected array $meta = [];

    protected ?string $successString = null;

    protected array $errors = [];

    protected ?string $message = null;

    protected int $status = 200;

    public function __construct(
        protected array $data = []
    ) {
    }

    #[Override]
    public function __toString(): string
    {
        return json_encode($this->toArray(), JSON_THROW_ON_ERROR);
    }

    public function success(string $success): static
    {
        $this->successString = $success;

        return $this;
    }

    public function errors(array $errors, string $message = null, int $status = 400): static
    {
        if ($message == null && isset(collect($errors)->values()[0])) {
            $message = collect($errors)
                ->values()[0];
        }

        if (is_array($message)) {
            $message = implode('. ', $message);
        }

        $this->message = $message;
        $this->errors = $errors;
        $this->status = $status;

        return $this;
    }

    public function addMeta(string $name, mixed $value): static
    {
        Arr::set($this->meta, $name, $value);

        return $this;
    }

    public function toArray(): array
    {
        $res = [];

        if ($this->data || ! $this->errors) {
            $res['data'] = $this->data;
        }

        if ($this->meta) {
            $res['meta'] = $this->meta;
        }

        if ($this->message) {
            $res['message'] = $this->message;
        }

        if ($this->errors) {
            $res['errors'] = $this->errors;
        }

        if ($this->successString) {
            $res['success'] = $this->successString;
        }

        return $res;
    }

    public function addMetas(array $meta): static
    {
        $this->meta = [...$this->meta, ...$meta];

        return $this;
    }

    public function download(string $contentType, string $filename): Response
    {
        $response = response($this->data, 200);
        $response->header('Content-Type', $contentType);
        $response->header('Content-Disposition', 'attachment; filename="' . $filename . '"');

        return $response;
    }

    public function response(): Response
    {
        return response($this->toArray(), $this->status);
    }
}
