<?php

namespace Datakode\LaravelDatakodeUtils;

use Datakode\LaravelDatakodeUtils\Controller\ApiInfoController;
use Datakode\LaravelDatakodeUtils\Controller\ProcessingsController;
use Illuminate\Routing\PendingResourceRegistration;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

class DatakodeRoute extends Route
{
    /**
     * Route an API resource to a controller.
     */
    public static function apiResource(
        string $name,
        string $controller,
        array $options = []
    ): PendingResourceRegistration {
        return Route::resource($name, $controller, $options)->except(['edit']);
    }

    /**
     * Route an API resource to a controller.
     */
    public static function apiResourceWithSoftDelete(
        string $name,
        string $controller,
        array $options = []
    ): PendingResourceRegistration {
        $res = self::apiResource($name, $controller, $options)->except('show');
        $res->register();

        $routeParamName = str($name)
            ->singular()
            ->slug('_')
            ->toString();
        Route::get('/' . $name . '/{' . $routeParamName . '}', [$controller, 'show'])
            ->name($name . '.show')->withTrashed();
        Route::put('/' . $name . '/{' . $routeParamName . '}/restore', [$controller, 'restore'])->name(
            $name . '.restore'
        );

        return $res;
    }

    /**
     * Register an array of API resource controllers.
     */
    public static function apiResources(array $resources, array $options = []): void
    {
        foreach ($resources as $name => $controller) {
            self::apiResource($name, $controller, $options);
        }
    }

    /**
     * Register an array of API resource controllers.
     */
    public static function apiResourcesWithSoftDelete(array $resources, array $options = []): void
    {
        foreach ($resources as $name => $controller) {
            self::apiResourceWithSoftDelete($name, $controller, $options);
        }
    }

    /**
     * Api home route route
     */
    public static function insertApiHomeRoute(): void
    {
        self::get('', [ApiInfoController::class, 'home'])->name('api.home');
        self::get('up', [ApiInfoController::class, 'healthCheck'])->name('api.health-check');
    }

    /**
     * Insert Processing routes
     */
    public static function insertProcessingRoutes(): void
    {
        self::apiResource('processings', ProcessingsController::class)->only('show', 'update', 'index');
        self::get('processings/{hash}/download', [ProcessingsController::class, 'download'])->name(
            'processings.download'
        );
    }

    /**
     * trim api base url base on route(api.home))
     * ex: http://127.0.0.1:8087/api/v1/processings/e39bf3fe4e4384ed7e17830e582c4f50/download -> processings/e39bf3fe4e4384ed7e17830e582c4f50/download
     */
    public static function trimApiBaseUrl(string $url): string
    {
        return Str::of($url)->replaceStart(route('api.home') . '/', '');
    }
}
