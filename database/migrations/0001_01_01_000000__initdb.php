<?php
/***
 *
 *  !!!! CETTE MIGRATION DOIT ÊTRE LA PREMIERE !!!!
 *
 *  Ajouter ici initialisation de la DB (extension, collations ...)
 *
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\PostgresConnection;
use Illuminate\Support\Facades\DB;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (DB::connection() instanceof PostgresConnection) {
            // initialize "unaccent" function @see https://en-wiki.ikoula.com/en/Adding_an_extension_in_PostgreSQL
            DB::statement('CREATE EXTENSION IF NOT EXISTS "unaccent"');

        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        // no down
    }
};
