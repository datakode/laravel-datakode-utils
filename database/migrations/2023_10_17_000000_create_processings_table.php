<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('processings', static function (Blueprint $table): void {
            $table->id();

            $table->unsignedSmallInteger('cpt');
            $table->unsignedSmallInteger('max');

            $table->longText('key');
            $table->string('hash')->unique()->index();
            $table->string('name')->nullable();
            $table->string('process_class');
            $table->json('parameters');
            $table->string('status')->nullable();
            $table->string('info')->nullable();
            $table->string('storage')->nullable();
            $table->string('filename')->nullable();
            $table->string('mime_type')->nullable();

            $table->boolean('mail_me')->default(false);
            $table->boolean('mail_sent')->default(false);

            $table->boolean('obsolete')->default(false);

            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->timestamp('started_at')->nullable();
            $table->timestamp('terminated_at')->nullable();
            $table->timestamp('downloaded_at')->nullable();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('processings');
    }
};
