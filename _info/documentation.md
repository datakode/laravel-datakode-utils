# Définir les resources data

Le formatage des json utilise la librairie [Laravel-data](https://spatie.be/docs/laravel-data/v4/introduction) les classes data doivent étendre `Datakode\LaravelDatakodeUtils\LaravelData`

Pour utiliser les [lazy-properties](https://spatie.be/docs/laravel-data/v4/as-a-resource/lazy-properties) il faut définir des enums définissant les scopes (ensemble d'includes) qui seront disponibles pour la ressource data.

exemple:

on a défini pour le projets les scopes suivants:

```php
<?php
// app/Data/DataScopesEnum.php

namespace App\Data;

enum DataScopesEnum
{
    case all;
    case withOrganizations;
}

```
Pour la ressource data on défini les lazy properties (qui ne seront pas inclus par défaut):
```php
<?php

namespace App\Data\User;

use App\Data\Credentials\CredentialsData;
use App\Data\DataScopesEnum;
use App\Data\Meta\MetaData;
use App\Data\Organization\OrganizationMinData;
use App\Models\User;
use Carbon\Carbon;
use Datakode\LaravelDatakodeUtils\LaravelData\DatakodeData;
use Spatie\LaravelData\Lazy;

/** @typescript */
class UserData extends DatakodeData
{
    public static string $dataName = User::class;

    protected static array $scopes = [
    // on définit quelles proprieties doivent être inclues selon le scope
      DataScopesEnum::withOrganizations->name =>['organization']
    ];

    public function __construct(
        public ?string                       $name,
        public string|Lazy                   $role,
        public OrganizationMinData|null|Lazy $organization,
        public array|Lazy                    $permissions,
        public bool|Lazy                     $deleted,
        public CredentialsData|Lazy          $credentials,
        public MetaData|Lazy                 $meta,
    )
    {
    }

    public static function fromModel(User $user): self
    {
        return new self(
            name: $user->name,
            role: Lazy::create(fn()=> $user->role->name),
            organization: Lazy::create(fn() => $user->organization ? OrganizationMinData::from(
                $user->organization
            ) : null 
            ),
            permissions: Lazy::create(fn() => $user->getPermissionsNames()->toArray()),
            deleted: Lazy::create(fn()=> $user->trashed()),
            credentials: Lazy::create(fn()=> CredentialsData::from($user)),
            meta: Lazy::create(fn()=> MetaData::from($user)),
        );
    }
}

```
Au niveau du controller si on charge la ressource sans définir de scope :
 ```php
public function show(User $user): array
    {
        return $this->showEntity($user, UserData::class);
    }
```
on n'obtient que les propriétés par default:
```json
{
    "data": {
        "name": " Test User"
    },
    ...
}
```
On peut définir le scope ainsi:

```php
public function show(User $user): array
    {
        return $this->showEntity($user, [UserData::class, DataScopesEnum::withOrganizations]);
    }
```
on n'obtient alors l'include des propriétés définies par ce scope:
```json
{
    "data": {
        "name": " Test User",
        "organization": {
            "name": "Test Commune",
            "type": "Commune"
        }        
    },
    ...
}
```
Cas particulier, si in scope à le name `all` (par exemple `DataScopesEnum::all`), alors toutes les lazy-properties de la ressource sont inclues.
exemple:


```php
public function show(User $user): array
    {
        return $this->showEntity($user, [UserData::class, DataScopesEnum::all]);
    }
```
```json
{
    "data": {
        "name": " Test User",
        "role": "user",
        "organization": {
            "id": 1,
            "k": "1",
            "t": "Test Commune (Commune)",
            "name": "Test Commune",
            "type": "Commune"
        },
        "permissions": [
            "profile_show",
            "profile_update",
            "profile_destroy"
        ],
        ...
    }
    },
    ...
}
```
