<?php

use Datakode\LaravelDatakodeUtils\DatakodeConfig;
use Datakode\LaravelDatakodeUtils\Jobs\ProcessExport;
use Datakode\LaravelDatakodeUtils\Model\Processing;

return [
    'auth' => [
        'model' => DatakodeConfig::configOrFail(
            'auth.providers.users.model'
        ), //ex: '\App\Models\User',  must extend Datakode\LaravelDatakodeUtils\Model\DatakodeUser
        'allow_guest_user' => true, //create virtual user with guest role when user is not logged
    ],

    'db_dump_cron' => env('DB_DUMP_CRON'), // cron text or null ie: 0 * * * *

    'debug_export' => env('DEBUG_EXPORT', false),

    'permissions' => [
        //  Access permissions

        'indexEntityTemplate' => '{model}_index',
        'listEntityTemplate' => '{model}_list',
        'showEntityTemplate' => '{model}_show',
        'storeEntityTemplate' => '{model}_store',
        'updateEntityTemplate' => '{model}_update',
        'destroyEntityTemplate' => '{model}_destroy',
        'restoreEntityTemplate' => '{model}_restore',
        'exportEntityTemplate' => '{model}_export',

        // Field permissions

        'readFieldPermissionTemplate' => '{model}_read_{field}',
        'writeFieldPermissionTemplate' => '{model}_write_{field}',

        // Debug

        'logPermissionChecks' => env('PERMISSIONS_LOG', false),
    ],

    'jobs' => [
        'ProcessExport' => ProcessExport::class,
    ],

    'processing' => [
        'model' => Processing::class,
        'webapp_download_url' => env(
            'WEB_APP_URL',
            'http://localhost:3000'
        ) . '/download/', // used for download link in end process email
        'via' => ['mail'],
    ],
    'exports' => [
        'max' => null, // define max entities number to export
        'pdf' => [
            'max' => 200, //max element to allow pdf export
        ],
    ],

    'disabled_fields' => env('DISABLED_FIELDS') ? explode(',', (string) env('DISABLED_FIELDS')) :
        [
            //            'technicalControl.pression',
        ],
];
