QA        = docker compose exec  -w "/var/www/packages/datakode/laravel-datakode-utils" -u www-data laravel

# This file allows one to run tests on docker like they do on gitlab.

##
## Tests
## -----
##

pest:
	$(QA) composer pest

pest-coverage:
	$(QA) composer pest-coverage

phpstan:
	$(QA) composer phpstan

##
## Quality assurance
## -----------------
##

ecs:
	$(QA) composer ecs

ecs-fix:
	$(QA) composer ecs-fix

rector-dry:
	$(QA) composer rector-dry

rector-hard:
	$(QA) composer rector-hard


## ------------------

before-commit:
	make composer-install
	make ecs
	make rector-dry
	make phpstan
	make pest

before-commit-fix-all:
	make composer-install
	make rector-hard
	make ecs-fix
	make rector-hard
	make ecs-fix
	make phpstan
	make pest


##
## composer
## -----------------
##

composer-install:
	$(QA) composer install

composer-update:
	$(QA) composer update

## ------------------

bash:
	$(QA) /bin/bash


.DEFAULT_GOAL := help
help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
.PHONY: help



