# Laravel-datakode-utils

[![pipeline status](https://gitlab.com/datakode/laravel-datakode-utils/badges/develop/pipeline.svg)](https://gitlab.com/datakode/laravel-datakode-utils/-/commits/develop) 
[![coverage report](https://gitlab.com/datakode/laravel-datakode-utils/badges/develop/coverage.svg)](https://gitlab.com/datakode/laravel-datakode-utils/-/commits/develop) 
[![Latest Release](https://gitlab.com/datakode/laravel-datakode-utils/-/badges/release.svg)](https://gitlab.com/datakode/laravel-datakode-utils/-/releases) 

# Installation

## Installation via packagist

```shell
 composer require datakode/laravel-datakode-utils 
```

## En développement, Installation comme git submobule 


```
mkdir -p packages/datakode
cd packages/datakode
git submodule add git@gitlab.com:datakode/laravel-datakode-utils.git
```

Modifier composer.json de votre projet

```php
"psr-4": {
            "App\\": "app/",            
            "Database\\Factories\\": "database/factories/",
            "Database\\Seeders\\": "database/seeders/",
            "Datakode\\LaravelDatakodeUtils\\": "packages/datakode/laravel-datakode-utils/src/"
        }
```
### Charger le submodule depuis un projet

```
git submodule update --init --recursive
```

## Charger le service provider

Registrer le service dans `app/Providers/AppServiceProvider.php`

```php
public function register(): void
    {
        $this->app->register(LaravelDatakodeUtilsServiceProvider::class);
    }
```

## Publier la config

```
php artisan vendor:publish
php artisan vendor:publish --provider="Datakode\LaravelDatakodeUtils\LaravelDatakodeUtilsServiceProvider"
```



-------
# Routes

Certaines routes sont nécessaires :

```php
<?php
// routes/api.php

DatakodeRoute::prefix('v1')->group(function () {

     DatakodeRoute::insertApiHomeRoute();
    
    // ...
    
    DatakodeRoute::insertProcessingRoutes(); // pour utiliser les exports
    
    // ...
    
});

```

---
# Documentation

[Documentation](./_info/documentation.md)
-------

![](https://gitlab.com/uploads/-/system/group/avatar/1091668/datakode.png?width=16)
[Datakode](https://www.datakode.fr/)

